ALTER TABLE
    `banco`
ADD
    `idusuario` INT NOT NULL
AFTER
    `banco`;

ALTER TABLE
    `banco`
ADD
    `datahora` DATETIME NULL DEFAULT NULL
AFTER
    `idusuario`;
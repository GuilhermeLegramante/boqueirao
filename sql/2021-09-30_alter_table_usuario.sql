ALTER TABLE boqueirao.usuario
 ADD login VARCHAR(20) NOT NULL AFTER email;

ALTER TABLE boqueirao.usuario
 ADD admin TINYINT NOT NULL AFTER senha;
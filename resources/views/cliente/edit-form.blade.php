@extends('adminlte::page')

@section('title', 'Clientes - Novo Cliente')

@section('content_header')

@endsection

@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><i class="fas fa-user-edit"></i> Edição Dados Cliente</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="fas fa-laptop-house"></i>
                            Início</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('clientes') }}"><i class="fas fa-users"></i>
                            Clientes</a></li>
                    <li class="breadcrumb-item active"><i class="fas fa-user-edit"></i> Edição Dados Cliente</li>
                </ol>
            </div>
        </div>
    </div>
</div>

@include('partials.flash-messages')

@livewire('cliente.edit-form-component', ['idcliente' => $idcliente])

@include('partials.float-menu.cadastros.cliente')

@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('css/styles.css') }}">
@stop

@section('js')
<script src="{{asset('js/scripts.js')}}"></script>
@stop
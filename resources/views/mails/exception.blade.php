@component('mail::message')

# Cliente: Boqueirão Remates
## Usuário: {{ session()->get('userId') }} - {{ session()->get('name') }}
## URL: {{ $content['url'] }}
## IP: {{ $content['ip'] }}

<p>{{ $content['message'] }} </p>

@endcomponent

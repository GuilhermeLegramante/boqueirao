<div>
    <div wire:ignore.self class="modal fade" id="modal-tipo-documento" role="dialog" data-keyboard="false"
        data-backdrop="static">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title"><i class="fas fa-file-alt"></i>
                        Tipo de Documento
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Descrição</label>
                                <input wire:model.lazy="descricao"
                                    wire:keydown.enter="{{ $isEdition ? 'update' : 'store' }}" type="text"
                                    maxlength="100"
                                    class="form-control input-custom {{ $errors->has('descricao') ? 'is-invalid' : '' }}">
                                @error('descricao')
                                    <h3 class="text-danger"><strong>{{ $message }}</strong>
                                    </h3>
                                @enderror
                            </div>
                        </div>
                    </div>

                    @include('partials.footer-crud', ['method' => '{{ $isEdition ? update : store }}'])

                </div>
            </div>
        </div>
    </div>
    @include('partials.modals.delete-form')
</div>

@push('scripts')
    <script>
        window.livewire.on('showModalTipoDocumento', () => {
            $('#modal-tipo-documento').modal('show');
        });
        window.livewire.on('closeModalTipoDocumento', () => {
            $('#modal-tipo-documento').modal('hide');
        });
        window.livewire.on('delete', () => {
            $('#modal-delete').modal('show');
        });
    </script>
@endpush

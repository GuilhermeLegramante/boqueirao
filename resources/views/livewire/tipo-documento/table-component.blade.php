<div>
    @include('partials.flash-messages')
    <div id="topo" class="card card-primary card-outline">
        <div class="card-body">
            <div class="row">
                <div wire:ignore class="col-sm-1">
                    <div class="form-group">
                        <label>Pag.</label>
                        <select id="perPage" class="form-control">
                            <option>10</option>
                            <option selected>30</option>
                            <option>50</option>
                            <option>75</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Busca pelo Código</label>
                        <input wire:model.debounce.300ms="searchCod" type="text" class="form-control"
                            placeholder="ex: 123">
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Busca pela Descrição</label>
                        <input wire:model.debounce.300ms="searchDesc" type="text" class="form-control"
                            placeholder="ex: ABC...">
                    </div>
                </div>

                <div class="col-sm-1">
                    <div class="form-group">
                        <label>Limpar</label><br>
                        <button wire:click="resetFields()" title="Limpar Filtros" style="width:100%;"
                            class=" btn btn-outline-primary">
                            <i class="fas fa-redo-alt"></i>
                        </button>
                    </div>
                </div>
            </div>

            @include('partials.spinner')

            <div class="table-responsive">
                <table style="padding: .3rem;" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th wire:click="sortBy('id')" style="text-align: left; width: 5%; cursor: pointer;">
                                Código
                                @include('partials.sort-icon', ['field' => 'id'])
                            </th>
                            <th wire:click="sortBy('descricao')" style="text-align: left; width: 40%; cursor: pointer;">
                                Descrição
                                @include('partials.sort-icon', ['field' => 'descricao'])
                            </th>
                            <th style="text-align: left; width: 5%;">Ações</th>
                    </thead>

                    <tbody>
                        @foreach ($data as $item)
                            <tr>
                                <td style="text-align: left; padding-left: 10px;">
                                    {{ $item->id }}
                                </td>
                                <td style="text-align: left;">
                                    {{ $item->descricao }}
                                </td>
                                <td>
                                    <div class="input-group">
                                        <button title="Editar o registro"
                                            wire:click="$emit('modalTipoDocumento', 'false', '{{ $item->id }}')"
                                            class="btn btn-info bg-gradient-success btn-xs mr-1"><i
                                                class="fas fa-edit"></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @if ($data->isEmpty())
                    <div class="d-flex justify-content-center">
                        Nenhum registro encontrado.
                    </div>
                @else

                    @if ($data->lastItem() != $data->total())
                        <div class="row">
                            <div class="col-12 text-center">
                                <a href="#topo" data-toggle="tooltip" title="VOLTAR AO TOPO"
                                    class="btn btn-outline-info btn-sm" style="color: #17a2b8;">
                                    <i class="fas fa-chevron-up"></i>
                                </a>
                                <a wire:click="load('30')" data-toggle="tooltip" title="VER MAIS"
                                    class="btn btn-outline-info btn-sm" style="color: #17a2b8;">
                                    <i class="fas fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                    @endif

                    <div class="d-flex mb-3">
                        <div class="mr-auto">
                            <p>
                                Mostrando de {{ $data->firstItem() }} até {{ $data->lastItem() }} de
                                {{ $data->total() }}
                                registros.
                            </p>
                        </div>
                        <div class="p-2">
                            <p>
                                {{ $data->links() }}
                            </p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @livewire('tipo-documento.form-component')
    @include('partials.float-menu.tipo-documento')
</div>

@push('scripts')
    <script src="{{ asset('js/livewire-scripts.js') }}"></script>

    <script>
        $('#perPage').select2();

        $('#perPage').on('change', function() {
            @this.perPage = $(this).val();
        })
    </script>
@endpush

<div>
    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-user"></i> Dados Pessoais</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body mb-n2">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Canal de Inclusão</label>
                        <select wire:model.lazy="canalinclusao" class="form-control">
                            <option></option>
                            <option value="DIVULGACAO">DIVULGAÇÃO</option>
                            <option value="RECINTO">RECINTO</option>
                            <option value="SITE">SITE</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nome Completo</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input placeholder="Fulano de Tal" wire:model.lazy="nome" maxlength="100" type="text" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}">
                            @error('nome')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>E-mail</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                            </div>
                            <input placeholder="fulano@detal.com" wire:model.lazy="email" type="text" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}">
                            @error('email')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Data de Nascimento</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input wire:model.lazy="datanascimento" type="date" class="form-control {{ $errors->has('datanascimento') ? 'is-invalid' : '' }}">
                            @error('datanascimento')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>CPF/CNPJ</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-file-alt"></i></span>
                            </div>
                            <input placeholder="Somente números" wire:model.lazy="cpfcnpj" type="text" class="form-control {{ $errors->has('cpfcnpj') ? 'is-invalid' : '' }}">
                            @error('cpfcnpj')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>RG</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-file-alt"></i></span>
                            </div>
                            <input placeholder="0234248439" wire:model.lazy="rg" type="text" class="form-control {{ $errors->has('rg') ? 'is-invalid' : '' }}">
                            @error('rg')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Celular (WhatsApp)</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                            </div>
                            <input wire:model="celularwhats" type="text" placeholder="55999999999" maxlength="14" class="form-control {{ $errors->has('celularwhats') ? 'is-invalid' : '' }}">
                            @error('celularwhats')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Celular</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                            </div>
                            <input wire:model.lazy="telefonecelular" type="text" placeholder="55999999999" maxlength="14" class="form-control {{ $errors->has('telefonecelular') ? 'is-invalid' : '' }}">
                            @error('telefonecelular')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Convencional 01</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                            </div>
                            <input wire:model.lazy="telefonecomercial" type="text" placeholder="5532519999" maxlength="13" class="form-control {{ $errors->has('telefonecomercial') ? 'is-invalid' : '' }}">
                            @error('telefonecomercial')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Convencional 02</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                            </div>
                            <input wire:model.lazy="telefoneresidencial" type="tel" placeholder="5532519999" maxlength="13" class="form-control {{ $errors->has('telefoneresidencial') ? 'is-invalid' : '' }}">
                            @error('telefoneresidencial')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nome da Mãe</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input placeholder="Fulana de Tal" wire:model.lazy="nomemae" maxlength="100" type="text" class="form-control {{ $errors->has('nomemae') ? 'is-invalid' : '' }}">
                            @error('nomemae')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nome do Pai</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input placeholder="Fulano de Tal" wire:model.lazy="nomepai" maxlength="100" type="text" class="form-control {{ $errors->has('nomepai') ? 'is-invalid' : '' }}">
                            @error('nomepai')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Profissão</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-toolbox"></i></span>
                            </div>
                            <input placeholder="Pecuarista" wire:model.lazy="profissao" maxlength="40" type="text" class="form-control">
                            @error('profissao')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Observação</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-file-alt"></i></span>
                            </div>
                            <input placeholder="Somente p/ autônomos" wire:model.lazy="obsprofissao" maxlength="40" type="text" class="form-control">
                            @error('obsprofissao')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Renda</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-coins"></i></span>
                            </div>
                            <input wire:model.lazy="renda" type="text" placeholder="2.500,00" maxlength="15" class="form-control {{ $errors->has('renda') ? 'is-invalid' : '' }}">
                            @error('renda')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-map-marked-alt"></i> Endereço</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body mb-n2">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>CEP &nbsp; <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei o CEP</a></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-marked-alt"></i></span>
                            </div>
                            <input wire:model.lazy="cep" placeholder="Somente números" type="text" class="form-control" size="10" maxlength="9">
                            @error('cep')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Rua/Avenida/Travessa</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                            </div>
                            <input placeholder="Rua, Av, Travessa..." wire:model.lazy="logradouro" type="text" class="form-control">
                            @error('logradouro')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Número</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-sort-numeric-up-alt"></i></span>
                            </div>
                            <input placeholder="123" wire:model.lazy="numero" type="text" class="form-control" maxlength="10">
                            @error('numero')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Complemento</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-house-user"></i></span>
                            </div>
                            <input placeholder="Apartamento, casa, bloco" wire:model.lazy="complemento" type="text" class="form-control">
                            @error('complemento')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bairro</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                            </div>
                            <input placeholder="Boqueirão" wire:model.lazy="bairro" type="text" class="form-control">
                            @error('bairro')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Cidade</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-map-marked-alt"></i></span>
                            </div>
                            <input placeholder="Santiago" wire:model.lazy="cidade" type="text" class="form-control">
                            @error('cidade')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Estado</label>
                        <select wire:model.lazy="uf" class="form-control">
                            <option></option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-money-check-alt"></i> Dados Bancários</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body mb-n2">
            <div class="row">
                <div class="col-sm-4">
                    <div wire:ignore class="form-group">
                        <label>Banco</label>
                        <select id="banco" class="form-control">
                            <option></option>
                            @foreach ($bancos as $item)
                            <option value="{{ $item->id }}">{{ $item->banco }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Agência</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-building"></i></span>
                            </div>
                            <input placeholder="1040" wire:model.lazy="agencia" maxlength="20" type="text" class="form-control">
                            @error('agencia')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Conta</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-money-check"></i></span>
                            </div>
                            <input placeholder="53653-6" wire:model.lazy="conta" maxlength="20" type="text" class="form-control">
                            @error('conta')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-file-alt"></i> Documentos</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Tipo de Documento</label>
                        <select wire:model="idTipoDocumento" class="form-control">
                            <option></option>
                            @foreach ($tiposDocumento as $item)
                            <option value="{{ $item['id'] }}">{{ $item['descricao'] }}</option>
                            @endforeach
                        </select>
                        @error('idTipoDocumento')
                        <h3 class="text-danger">
                            <strong>{{ $message }}</strong>
                        </h3>
                        @enderror
                    </div>
                </div>

                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Documento</label>
                        <input type="file" wire:model="arquivo" class="form-control">
                        @error('arquivo')
                        <h3 class="text-danger">
                            <strong>{{ $message }}</strong>
                        </h3>
                        @enderror
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                        <label> Ação </label> <br>
                        <button type="button" wire:click.prevent="storeDocumento()" class="btn btn-outline-primary btn-sm">
                            <strong> INCLUIR &nbsp;</strong>
                            <i class="fas fa-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>

            @if (count($documentos) > 0)
            <div class="table-responsive">
                <table style="padding: .3rem;" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th style="text-align: left; width: 40%; cursor: pointer;">
                                Tipo de Documento
                            </th>
                            <th style="text-align: left; width: 40%; cursor: pointer;">
                                Arquivo
                            </th>
                            <th style="text-align: left; width: 5%;"><i class="fas fa-cogs"></i> Ações</th>
                    </thead>

                    <tbody>
                        @foreach ($documentos as $key => $item)
                        <tr>
                            <td style="text-align: left;">{{ $item['descricaoTipoDocumento'] }}</td>
                            <td style="text-align: left;">{{ $item['nomeArquivo'] }}</td>
                            <td>
                                <div class="input-group">
                                    <button data-toggle="tooltip" data-placement="top" title="Excluir o registro" wire:click="deleteDocumento({{ $key }})" class="btn bg-gradient-danger btn-xs"><i class="fas fa-times"></i></button>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @endif
        </div>
    </div>

    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-laptop-house"></i> Informações Adicionais</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Cadastro em outra leiloeira?</label>
                        <select wire:model.lazy="cadastroleiloeira" class="form-control">
                            <option value="0">NÃO</option>
                            <option value="1">SIM</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Perfil</label>
                        <select wire:model.lazy="perfil" class="form-control">
                            <option></option>
                            <option value="C">COMPRA</option>
                            <option value="V">VENDA</option>
                            <option value="CV">COMPRA E VENDA</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Leiloeira</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-gavel"></i></span>
                            </div>
                            <input placeholder="Outra leiloeira" wire:model.lazy="leiloeira" maxlength="40" type="text" class="form-control">
                            @error('leiloeira')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Estabelecimento</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-house-user"></i></span>
                            </div>
                            <input placeholder="Nome do estabelecimento do cliente" wire:model.lazy="estabelecimento" maxlength="40" type="text" class="form-control">
                            @error('estabelecimento')
                            <span class="error invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label>Situação</label>
                        <select wire:model.lazy="situacao" class="form-control">
                            <option></option>
                            <option>HABILITADO</option>
                            <option>INABILITADO</option>
                            <option>INATIVO</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Observação</label>
                        <textarea wire:model.lazy="observacao" class="form-control" cols="30" rows="4"></textarea>
                        @error('observacao')
                        <span class="error invalid-feedback">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </div>



    <br><br>

    @include('partials.spinner')

    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-auto">
                <a href="{{ route('clientes') }}" class="btn btn-outline-info btn-sm"><i class="fas fa-times"></i>&nbsp;
                    <strong>CANCELAR</strong> </a>

                <button type="button" wire:click.prevent="store" class="btn btn-outline-success btn-sm">
                    <strong> SALVAR &nbsp;</strong>
                    <i class="fas fa-save"></i>
                </button>
            </div>
        </div>
    </div>
    <br>

</div>


@push('scripts')
<script>
    $('#canalinclusao').select2();

    $('#canalinclusao').on('change', function() {
        @this.canalinclusao = $(this).val();
    });

    $('#uf').select2({
        language: "pt-BR"
        , allowClear: true
        , placeholder: "Selecione..."
    });

    $('#uf').on('change', function() {
        @this.uf = $(this).val();
    });

    $('#banco').select2({
        language: "pt-BR"
        , allowClear: true
        , placeholder: "Selecione..."
    });

    $('#banco').on('change', function() {
        @this.banco = $(this).val();
    });

    $('#cadastroleiloeira').select2();

    $('#cadastroleiloeira').on('change', function() {
        @this.cadastroleiloeira = $(this).val();
    });

    $('#situacao').select2({
        language: "pt-BR"
        , allowClear: true
        , placeholder: "Selecione..."
    });

    $('#situacao').on('change', function() {
        @this.situacao = $(this).val();
    });

</script>
@endpush

<div wire:ignore.self class="modal fade" id="modal-delete" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h3 class="modal-title">ATENÇÃO <i class="fas fa-exclamation-triangle"></i></h3>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                @include('partials.spinner')
                <div class="card-body p-0">
                    <div class="form-group">
                        <label>Cliente</label>
                        <h6 class="form-control bg-light">{{$nome}}</h6>
                    </div>
                </div>
                Deseja realmente excluir o Cliente? <br>
                <label>OBS: Esta ação não poderá ser desfeita.</label>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info btn-sm" data-dismiss="modal"><i class="fas fa-times"
                        aria-hidden="true"></i>&nbsp; <strong>FECHAR</strong> </button>

                <button type="button" wire:click.prevent="destroy()" class="btn btn-outline-danger btn-sm">
                    <strong> EXCLUIR &nbsp;</strong>
                    <i class="fas fa-trash-alt" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>
</div>
@livewireScripts
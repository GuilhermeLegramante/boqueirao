<div id="topo" class="card card-primary card-outline">
    <div class="card-body mb-n2">
        <div class="row">
            <div wire:ignore class="col-sm-1">
                <div class="form-group">
                    <label>Pag.</label>
                    <select id="perPage" class="form-control">
                        <option>10</option>
                        <option>30</option>
                        <option>50</option>
                        <option>100</option>
                    </select>
                </div>
            </div>

            <div class="col-md-10">
                <div class="form-group">
                    <label>Busca pelo Nome ou pelo CPF/CNPJ</label>
                    <input wire:model.debounce.300ms="search" type="text" class="form-control" placeholder="">
                </div>
            </div>

            <div class="col-sm-1">
                <div class="form-group">
                    <label>Limpar</label><br>
                    <a wire:click="cleanFilters()" href="" title="Limpar Filtros" style="width: 100%;" class="btn btn-outline-primary"><i class="fas fa-redo-alt"></i></a>
                </div>
            </div>
        </div>

        @include('partials.spinner')

        <div class="table-responsive">
            <table style="padding: .3rem;" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th wire:click="sortBy('idcliente')" style="text-align: left; width: 5%; cursor: pointer;">
                            <i class="fas fa-sort-numeric-up-alt"></i>
                            Código
                            @include('partials.sort-icon', ['field' => 'idcliente'])
                        </th>
                        <th wire:click="sortBy('cpfcnpj')" style="text-align: left; width: 10%; cursor: pointer;">
                            <i class="fas fa-sort-numeric-up-alt"></i>
                            CPF/CNPJ
                            @include('partials.sort-icon', ['field' => 'cpfcnpj'])
                        </th>
                        <th wire:click="sortBy('nome')" style="text-align: left; width: 60%; cursor: pointer;">
                            <i class="fas fa-user"></i>
                            Nome
                            @include('partials.sort-icon', ['field' => 'nome'])
                        </th>
                        <th wire:click="sortBy('email')" style="text-align: left; width: 20%; cursor: pointer;">
                            <i class="fas fa-envelope"></i>
                            E-mail
                            @include('partials.sort-icon', ['field' => 'email'])
                        </th>
                        <th wire:click="sortBy('datanascimento')" style="text-align: left; width: 20%; cursor: pointer;">
                            <i class="fas fa-calendar-alt"></i>
                            Dt Nasc
                            @include('partials.sort-icon', ['field' => 'datanascimento'])
                        </th>
                        <th wire:click="sortBy('datacadastro')" style="text-align: left; width: 20%; cursor: pointer;">
                            <i class="fas fa-calendar-alt"></i>
                            Cadastro
                            @include('partials.sort-icon', ['field' => 'datacadastro'])
                        </th>
                        <th wire:click="sortBy('situacao')" style="text-align: left; width: 10%; cursor: pointer;">
                            <i class="fas fa-user-lock"></i>
                            Situação
                            @include('partials.sort-icon', ['field' => 'situacao'])
                        </th>
                        <th wire:click="sortBy('canalinclusao')" style="text-align: left; width: 10%; cursor: pointer;">
                            <i class="fas fa-user-shield"></i>
                            Canal
                            @include('partials.sort-icon', ['field' => 'canalinclusao'])
                        </th>
                        <th style="text-align: left; width: 5%;"><i class="fas fa-cogs"></i> Ações</th>
                </thead>

                <tbody>
                    @foreach($clientes as $item)
                    <tr>
                        <td style="text-align: left;">
                            {{ str_pad($item->idcliente, 5, "0", STR_PAD_LEFT) }}</td>
                        <td style="text-align: center;">{{ $item->cpfcnpj }}</td>
                        <td style="text-align: left;">
                            {{ mb_strtoupper(substr($item->nome, 0, 50), 'UTF-8') }}
                        </td>
                        <td style="text-align: left;">{{ $item->email }}</td>
                        <td>
                            {{ date('d/m/Y', strtotime($item->datanascimento)) }}
                        </td>
                        <td>
                            {{ date('d/m/Y', strtotime($item->datacadastro)) }}
                        </td>
                        <td>
                            @if ($item->situacao == 'HABILITADO')
                            <span class="badge bg-success float-center custom-badge">HABILITADO</span>
                            @endif
                            @if ($item->situacao == 'INABILITADO')
                            <span class="badge bg-warning float-center custom-badge">INABILITADO</span>
                            @endif
                            @if (($item->situacao == 'INATIVO') || ($item->situacao == ''))
                            <span class="badge bg-danger float-center custom-badge">INATIVO</span>
                            @endif
                        </td>
                        <td>
                            @if ($item->canalinclusao == 'RECINTO')
                            <span class="badge bg-info float-center custom-badge">RECINTO</span>
                            @endif
                            @if ($item->canalinclusao == 'SITE')
                            <span class="badge bg-secondary float-center custom-badge">SITE</span>
                            @endif
                            @if ($item->canalinclusao == 'DIVULGACAO')
                            <span class="badge bg-primary float-center custom-badge">DIVULGAÇÃO</span>
                            @endif
                        </td>

                        <td>
                            <div class="input-group">
                                <a href="{{ route('clientes.viewEditForm', $item->idcliente) }}" title="Editar o registro" class="btn btn-info bg-gradient-success btn-xs mr-1"><i class="fas fa-edit"></i></a>

                                <a href="{{ route('clientes.getBasicReport', $item->idcliente)}}" title="Gerar PDF" target="_blank" class="btn btn-info bg-gradient-info btn-xs mr-1"><i class="far fa-file-alt"></i></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            @if ($clientes->isEmpty())
            <div class="d-flex justify-content-center">
                Nenhum registro encontrado.
            </div>
            @else

            @if($clientes->lastItem() != $clientes->total())
            <div class="row">
                <div class="col-12 text-center">
                    <a href="#topo" title="VOLTAR AO TOPO" class="btn btn-outline-info btn-sm" style="color: #17a2b8;">
                        <i class="fas fa-chevron-up"></i>
                    </a>
                    <a wire:click="load('30')" title="VER MAIS" class="btn btn-outline-info btn-sm" style="color: #17a2b8;">
                        <i class="fas fa-chevron-down"></i>
                    </a>
                </div>
            </div>
            @endif

            <div class="d-flex mb-3">
                <div class="mr-auto">
                    <p>
                        Mostrando de {{ $clientes->firstItem()}} até {{ $clientes->lastItem() }} de
                        {{ $clientes->total() }}
                        registros.
                    </p>
                </div>
                <div class="p-2">
                    <p>
                        {{ $clientes->links() }}
                    </p>
                </div>
            </div>
            @endif
        </div>

    </div>
</div>
@livewireScripts

@push('scripts')
<script>
    $('#perPage').select2();

    $('#perPage').on('change', function() {
        @this.perPage = $(this).val();
    })

</script>
@endpush

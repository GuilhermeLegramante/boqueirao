<div wire:key="cliente" wire:ignore.self class="modal fade" id="modal-add" role="dialog" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title"><i class="fas fa-file-alt"></i>
                    Novo Documento
                </h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @include('partials.spinner')
                <form wire:submit.prevent="store">
                    <div class="row">
                        <div class="col-sm-12">
                            <div wire:ignore class="form-group">
                                <label>Tipo de Documento</label>
                                <select id="tipodocumento" class="form-control">
                                    <option></option>
                                    @foreach ($tiposDocumento as $item)
                                    <option value="{{$item->id}}">{{$item->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div wire:ignore class="form-group">
                                <label>Cliente</label>
                                <select id="cliente" class="form-control">
                                    <option></option>
                                    @foreach ($clientes as $item)
                                    <option value="{{$item->idcliente}}">{{$item->nome}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Documento</label>
                                <input type="file" wire:model="documento" class="form-control">
                                @error('documento') <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info btn-sm" data-dismiss="modal"><i class="fas fa-times"
                        aria-hidden="true"></i>&nbsp; <strong>FECHAR</strong> </button>

                <button type="button" wire:click.prevent="store()"
                    class="btn btn-outline-success btn-sm">
                    <strong> SALVAR &nbsp;</strong>
                    <i class="fas fa-save" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>
</div>
@livewireScripts

@push('scripts')
<script>
    $('#cliente').select2({
        language: "pt-BR",
        allowClear: true,
        placeholder: "Selecione..."
    });

    $('#cliente').on('change', function(){
        @this.cliente = $(this).val();
    });

    $('#tipodocumento').select2({
        language: "pt-BR",
        allowClear: true,
        placeholder: "Selecione..."
    });

    $('#tipodocumento').on('change', function(){
        @this.tipodocumento = $(this).val();
    });

</script>
@endpush
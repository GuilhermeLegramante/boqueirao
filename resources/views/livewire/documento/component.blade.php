<div>
    @include('partials.flash-messages')
    @include('livewire.documento.form')
    @include('livewire.documento.table')
    @include('livewire.documento.confirm-delete')
</div>

@push('scripts')
<script>
    window.livewire.on('store', () => {
        $('#modal-add').modal('hide');
    });
    window.livewire.on('delete', () => {
        $('#modal-delete').modal('hide');
    });
</script>
@endpush
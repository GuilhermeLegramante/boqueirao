<div id="topo" class="card card-primary card-outline">
    <div class="card-body mb-n2">
        <div class="row">
            <div wire:ignore class="col-sm-1">
                <div class="form-group">
                    <label>Pag.</label>
                    <select id="perPage" class="form-control">
                        <option>10</option>
                        <option>25</option>
                        <option>50</option>
                        <option>100</option>
                    </select>
                </div>
            </div>

            <div class="col-md-10">
                <div class="form-group">
                    <label>Busca pelo Cliente</label>
                    <input wire:model.debounce.300ms="searchCliente" type="text" class="form-control"
                        placeholder="ex: João Silva">
                </div>
            </div>

            <div class="col-sm-1">
                <div class="form-group">
                    <label>Limpar</label><br>
                    <a wire:click="cleanFilters()" href="" title="Limpar Filtros" style="width: 100%;"
                        class="btn btn-outline-primary"><i class="fas fa-redo-alt"></i></a>
                </div>
            </div>
        </div>

        @include('partials.spinner')

        <div class="table-responsive">
            <table style="padding: .3rem;" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th wire:click="sortBy('iddocumento')" style="text-align: left; width: 5%; cursor: pointer;">
                            <i class="fas fa-sort-numeric-up-alt"></i>
                            Código
                            @include('partials.sort-icon', ['field' => 'iddocumento'])
                        </th>
                        <th wire:click="sortBy('descricaotipodocumento')"
                            style="text-align: left; width: 40%; cursor: pointer;">
                            <i class="fas fa-file-alt"></i>
                            Descrição
                            @include('partials.sort-icon', ['field' => 'descricaotipodocumento'])
                        </th>
                        <th wire:click="sortBy('nomecliente')" style="text-align: left; width: 40%; cursor: pointer;">
                            <i class="fas fa-user"></i>
                            Cliente
                            @include('partials.sort-icon', ['field' => 'nomecliente'])
                        </th>
                        <th wire:click="sortBy('datainclusao')" style="text-align: left; width: 20%; cursor: pointer;">
                            <i class="fas fa-calendar-alt"></i>
                            Data Inclusão
                            @include('partials.sort-icon', ['field' => 'datainclusao'])
                        </th>

                        <th style="text-align: left; width: 5%;"><i class="fas fa-cogs"></i> Ações</th>
                </thead>

                <tbody>
                    @foreach($documentos as $item)
                    <tr>
                        <td style="text-align: left;">
                            {{ str_pad($item->iddocumento, 5, "0", STR_PAD_LEFT) }}</td>
                        <td style="text-align: left;">
                            {{ mb_strtoupper(substr($item->descricaotipodocumento, 0, 50), 'UTF-8') }}
                        </td>
                        <td style="text-align: left;">
                            {{ mb_strtoupper(substr($item->nomecliente, 0, 50), 'UTF-8') }}
                        </td>
                        <td>
                            {{ date('d/m/Y \à\s H:i:s', strtotime($item->datainclusao)) }}
                        </td>

                        <td>
                            <div class="input-group">
                                <a href="{{URL::to('/storage/'. $item->path)}}" target="_blank" title="Download do documento"
                                    class="btn btn-info bg-gradient-info btn-xs mr-1"><i
                                        class="fas fa-file-download"></i></a>

                                <button data-toggle="modal" data-toggle="tooltip" data-placement="top"
                                    title="Excluir o registro" data-target="#modal-delete"
                                    wire:click="delete({{ $item->iddocumento }})"
                                    class="btn btn-info bg-gradient-danger btn-xs"><i
                                        class="fas fa-trash-alt"></i></button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            @if ($documentos->isEmpty())
            <div class="d-flex justify-content-center">
                Nenhum registro encontrado.
            </div>
            @else

            @if($documentos->lastItem() != $documentos->total())
            <div class="row">
                <div class="col-12 text-center">
                    <a href="#topo" data-toggle="tooltip" title="VOLTAR AO TOPO" class="btn btn-outline-info btn-sm"
                        style="color: #17a2b8;">
                        <i class="fas fa-chevron-up"></i>
                    </a>
                    <a wire:click="load('30')" data-toggle="tooltip" title="VER MAIS"
                        class="btn btn-outline-info btn-sm" style="color: #17a2b8;">
                        <i class="fas fa-chevron-down"></i>
                    </a>
                </div>
            </div>
            @endif

            <div class="d-flex mb-3">
                <div class="mr-auto">
                    <p>
                        Mostrando de {{ $documentos->firstItem()}} até {{ $documentos->lastItem() }} de
                        {{ $documentos->total() }}
                        registros.
                    </p>
                </div>
                <div class="p-2">
                    <p>
                        {{ $documentos->links() }}
                    </p>
                </div>
            </div>
            @endif
        </div>

    </div>
</div>

@push('scripts')
<script>
    $('[data-toggle="tooltip"]').tooltip();
    $('#perPage').select2();

    $('#perPage').on('change', function(){
        @this.perPage = $(this).val();
    })
</script>
@endpush
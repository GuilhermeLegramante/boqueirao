<div>
    <div wire:ignore.self class="modal fade" id="modal-bank" role="dialog" data-keyboard="false"
        data-backdrop="static">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h5 class="modal-title"><i class="fas fa-building"></i>
                        Banco
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Código</label>
                                <input wire:model.lazy="code" type="number" maxlength="100"
                                    class="form-control input-custom {{ $errors->has('code') ? 'is-invalid' : '' }}">
                                @error('code')
                                    <h3 class="text-danger"><strong>{{ $message }}</strong>
                                    </h3>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Descrição</label>
                                <input wire:model.lazy="description" wire:keydown.enter="store" type="text"
                                    maxlength="100"
                                    class="form-control input-custom {{ $errors->has('description') ? 'is-invalid' : '' }}">
                                @error('description')
                                    <h3 class="text-danger"><strong>{{ $message }}</strong>
                                    </h3>
                                @enderror
                            </div>
                        </div>
                    </div>

                    @if ($isEdition == false)
                        @include('partials.footer-crud', ['method' => 'store'])
                    @else
                        @include('partials.footer-crud', ['method' => 'update'])
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('partials.modals.delete-form')
</div>

@push('scripts')
    <script>
        window.livewire.on('showModalBank', () => {
            $('#modal-bank').modal('show');
        });
        window.livewire.on('closeModalBank', () => {
            $('#modal-bank').modal('hide');
        });
        window.livewire.on('delete', () => {
            $('#modal-delete').modal('show');
        });
    </script>
@endpush

<div>
    @include('partials.flash-messages')
    @include('livewire.cadastros.cliente-form')
    @include('livewire.cadastros.cliente-table')
    @include('livewire.cadastros.cliente-confirm-delete')
</div>

@push('scripts')
<script>
    window.livewire.on('store', () => {
        $('#modal-add').modal('hide');
    });
    window.livewire.on('delete', () => {
        $('#modal-delete').modal('hide');
    });
</script>
@endpush

<div wire:key="cliente" wire:ignore.self class="modal fade" id="modal-add" role="dialog" data-keyboard="false"
    data-backdrop="static">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title"><i class="fas fa-user-plus"></i>
                    {{ $updateMode ? 'Edição - Cliente' : 'Novo Cliente' }}
                </h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body">
                @include('partials.spinner')
                <form wire:submit.prevent="store">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nome Completo</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input placeholder="Fulano de Tal" wire:model.debounce.300ms="nome" maxlength="100"
                                        type="text" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}">
                                    @error('nome') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>E-mail</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                    </div>
                                    <input placeholder="fulano@detal.com" wire:model.debounce.300ms="email" type="text"
                                        class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}">
                                    @error('email') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>CPF/CNPJ</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-file-alt"></i></span>
                                    </div>
                                    <input placeholder="Somente números" wire:model.debounce.300ms="cpfcnpj" type="text"
                                        class="form-control {{ $errors->has('cpfcnpj') ? 'is-invalid' : '' }}">
                                    @error('cpfcnpj') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>RG</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-file-alt"></i></span>
                                    </div>
                                    <input placeholder="0234248439" wire:model.debounce.300ms="rg" type="text"
                                        class="form-control {{ $errors->has('rg') ? 'is-invalid' : '' }}">
                                    @error('rg') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nome da Mãe</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input placeholder="Fulana de Tal" wire:model.debounce.300ms="nomemae"
                                        maxlength="100" type="text"
                                        class="form-control {{ $errors->has('nomemae') ? 'is-invalid' : '' }}">
                                    @error('nomemae') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nome do Pai</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                                    </div>
                                    <input placeholder="Fulano de Tal" wire:model.debounce.300ms="nomepai"
                                        maxlength="100" type="text"
                                        class="form-control {{ $errors->has('nomepai') ? 'is-invalid' : '' }}">
                                    @error('nomepai') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Data de Nascimento</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-calendar-alt"></i></span>
                                    </div>
                                    <input wire:model.debounce.300ms="datanascimento" type="date"
                                        class="form-control {{ $errors->has('datanascimento') ? 'is-invalid' : '' }}">
                                    @error('datanascimento') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Telefone Comercial</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input wire:model.debounce.300ms="telefonecomercial" type="text"
                                        placeholder="5532519999" maxlength="10"
                                        class="form-control {{ $errors->has('telefonecomercial') ? 'is-invalid' : '' }}">
                                    @error('telefonecomercial') <span
                                        class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Telefone Residencial</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input wire:model.debounce.300ms="telefoneresidencial" type="tel"
                                        placeholder="5532519999" maxlength="10"
                                        class="form-control {{ $errors->has('telefoneresidencial') ? 'is-invalid' : '' }}">
                                    @error('telefoneresidencial') <span
                                        class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Telefone Celular</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                    </div>
                                    <input wire:model.debounce.300ms="telefonecelular" type="tel"
                                        placeholder="55999999999" maxlength="11"
                                        class="form-control {{ $errors->has('telefonecelular') ? 'is-invalid' : '' }}">
                                    @error('telefonecelular') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>CEP &nbsp; <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/"
                                        target="_blank">Não sei o CEP</a></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-map-marked-alt"></i></span>
                                    </div>
                                    <input wire:model.debounce.300ms="cep" placeholder="Somente números" type="text"
                                        class="form-control" size="10" maxlength="9">
                                    @error('cep') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Logradouro</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                    </div>
                                    <input placeholder="Rua dos Remates" wire:model.debounce.300ms="logradouro"
                                        type="text" class="form-control">
                                    @error('logradouro') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Número</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-sort-numeric-up-alt"></i></span>
                                    </div>
                                    <input placeholder="123" wire:model.debounce.300ms="numero" type="text"
                                        class="form-control" maxlength="10">
                                    @error('numero') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Complemento</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-house-user"></i></span>
                                    </div>
                                    <input placeholder="Apartamento, casa, bloco"
                                        wire:model.debounce.300ms="complemento" type="text" class="form-control">
                                    @error('complemento') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Bairro</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                    </div>
                                    <input placeholder="Boqueirão" wire:model.debounce.300ms="bairro" type="text"
                                        class="form-control">
                                    @error('bairro') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>Cidade</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-map-marked-alt"></i></span>
                                    </div>
                                    <input placeholder="Santiago" wire:model.debounce.300ms="cidade" type="text"
                                        class="form-control">
                                    @error('cidade') <span class="error invalid-feedback">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div wire:ignore class="form-group">
                                <label>Estado</label>
                                <select id="uf" name="uf" class="form-control">
                                    <option></option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div wire:ignore class="form-group">
                                <label>Banco</label>
                                <select id="banco" class="form-control">
                                    <option></option>
                                    @foreach ($bancos as $item)
                                    <option value="{{$item->id}}">{{$item->banco}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Agência</label>
                                <input placeholder="1040" wire:model.debounce.300ms="agencia" maxlength="20" type="text"
                                    class="form-control">
                                @error('agencia') <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Conta</label>
                                <input placeholder="53653-6" wire:model.debounce.300ms="conta" maxlength="20"
                                    type="text" class="form-control">
                                @error('conta') <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Profissão</label>
                                <input placeholder="Pecuarista" wire:model.debounce.300ms="profissao" maxlength="40"
                                    type="text" class="form-control">
                                @error('profissao') <span class="error invalid-feedback">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div wire:ignore class="form-group">
                                <label>Faixa de Renda</label>
                                <select id="faixarenda" class="form-control">
                                    <option></option>
                                    @foreach ($faixasrenda as $item)
                                    <option value="{{$item->id}}">De {{$item->valorinicial}} a {{$item->valorfinal}}
                                        salários</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div wire:ignore class="form-group">
                                <label>Leiloeira</label>
                                <select id="leiloeira" class="form-control">
                                    <option></option>
                                    @foreach ($leiloeiras as $item)
                                    <option value="{{$item->id}}">{{$item->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div wire:ignore class="form-group">
                                <label>Habilitado</label>
                                <select id="habilitado" class="form-control">
                                    <option></option>
                                    <option value="0">NÃO</option>
                                    <option value="1">SIM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div wire:ignore class="form-group">
                                <label>Estabelecimento</label>
                                <select id="estabelecimento" class="form-control">
                                    <option></option>
                                    @foreach ($estabelecimentos as $item)
                                    <option value="{{$item->id}}">{{$item->descricao}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Comprovante do Documento</label>
                                <input type="file" wire:model="comprovantedocumento" class="form-control">
                                @error('comprovantedocumento') <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Comprovante de Endereço</label>
                                <input type="file" wire:model="comprovanteendereco" class="form-control">
                                @error('comprovanteendereco') <span class="error">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-info btn-sm" data-dismiss="modal"><i class="fas fa-times"
                        aria-hidden="true"></i>&nbsp; <strong>FECHAR</strong> </button>

                <button type="button" wire:click.prevent="{{ $updateMode ? 'update()': 'store()'}}"
                    class="btn btn-outline-success btn-sm">
                    <strong> SALVAR &nbsp;</strong>
                    <i class="fas fa-save" aria-hidden="true"></i>
                </button>
            </div>
        </div>
    </div>
</div>
@livewireScripts

@push('scripts')
<script>
    $('#uf').select2({
        language: "pt-BR",
        allowClear: true,
        placeholder: "Selecione..."
    });

    $('#uf').on('change', function(){
        @this.uf = $(this).val();
    });

    $('#banco').select2({
        language: "pt-BR",
        allowClear: true,
        placeholder: "Selecione..."
    });

    $('#banco').on('change', function(){
        @this.banco = $(this).val();
    });

    $('#faixarenda').select2({
        language: "pt-BR",
        allowClear: true,
        placeholder: "Selecione..."
    });

    $('#faixarenda').on('change', function(){
        @this.faixarenda = $(this).val();
    });

    $('#leiloeira').select2({
        language: "pt-BR",
        allowClear: true,
        placeholder: "Selecione..."
    });

    $('#leiloeira').on('change', function(){
        @this.leiloeira = $(this).val();
    });

    $('#habilitado').select2({
        language: "pt-BR",
        allowClear: true,
        placeholder: "Selecione..."
    });

    $('#habilitado').on('change', function(){
        @this.habilitado = $(this).val();
    });

    $('#estabelecimento').select2({
        language: "pt-BR",
        allowClear: true,
        placeholder: "Selecione..."
    });

    $('#estabelecimento').on('change', function(){
        @this.estabelecimento = $(this).val();
    });

</script>
@endpush
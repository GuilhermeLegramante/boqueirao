<div class="row">
    @include('partials.inputs.number', [
        'columnSize' => 12,
        'label' => 'Código*',
        'model' => 'code',
    ])
</div>
<div class="row">
    @include('partials.inputs.text', [
        'columnSize' => 12,
        'label' => 'Descrição*',
        'model' => 'description',
        'maxLenght' => 100,
    ])
</div>
<p><small>*campos obrigatórios</small></p>

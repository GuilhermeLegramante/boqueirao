<div class="modal-footer">
    <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal"><i class="fas fa-times"
            aria-hidden="true"></i>
        <strong> CANCELAR &nbsp;</strong></button>
    <button wire:click.prevent="{{ $method }}" type="submit" wire:loading.class="disabled"
        class="btn btn-primary btn-sm">
        <strong> SALVAR &nbsp;</strong>
        <i class="fas fa-save" aria-hidden="true"></i>
    </button>
    @if ($method == 'update')
        <button wire:click="showModalDelete()" data-placement="top" title="Excluir o registro"
            class="btn btn-outline-danger btn-sm">
            <strong> EXCLUIR &nbsp;</strong>
            <i class="fas fa-trash-alt"></i>
        </button>
    @endif
    @include('partials.spinner-min')
</div>
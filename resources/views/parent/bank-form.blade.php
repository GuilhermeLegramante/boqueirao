@extends('adminlte::page')

@section('content')
    <div>
        @livewire('bank-form', ['id' => $id])
    </div>
@endsection

@extends('adminlte::page')

@section('content')
    <div>
        @livewire('document-type-form', ['id' => $id])
    </div>
@endsection

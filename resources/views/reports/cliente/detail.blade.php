@extends('reports.page')

@section('header')
@include('partials.reports.header')
@endsection

@section('content')
<h1 style="margin-left: 1%; font-size: 23px;">{{ $cliente->nome }}</h1>
<p style="margin-left: 1%; margin-top: -1%"><strong>Data do Cadastro: </strong>{{ date('d/m/Y', strtotime($cliente->datacadastro)) }} <strong> Data última
        alteração: </strong>{{ date('d/m/Y \à\s H:i:s', strtotime($cliente->dataalteracao)) }}</p>
<br>
<h1 style="margin-left: 1%;">Dados Pessoais</h1>
<table>
    <tbody>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>Nome:</strong> {{ $cliente->nome }}</td>
            <td class="collumn-right"><strong>E-mail:</strong> {{ $cliente->email }}</td>
        </tr>
        <tr class="" style="font-size: 13px;">
            <td class="collumn-left"><strong>CPF/CNPJ:</strong> {{ $cliente->cpfcnpj }}</td>
            <td class="collumn-right"><strong>RG:</strong> {{ $cliente->rg }}</td>
        </tr>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>Nome da Mãe:</strong> {{ $cliente->nomemae }}</td>
            <td class="collumn-right"><strong>Nome do Pai:</strong> {{ $cliente->nomepai }}</td>
        </tr>
        <tr class="" style="font-size: 13px;">
            <td class="collumn-left"><strong>Data de Nascimento:</strong>
                {{ date('d/m/Y', strtotime($cliente->datanascimento)) }}</td>
            <td class="collumn-right"><strong>Telefone Comercial:</strong> {{ $cliente->telefonecomercial }}</td>
        </tr>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>Telefone Residencial</strong> {{ $cliente->telefoneresidencial }}</td>
            <td class="collumn-right"><strong>Telefone Celular:</strong> {{ $cliente->telefonecelular }}</td>
        </tr>
        <tr class="" style="font-size: 13px;">
            <td class="collumn-left"><strong>Profissão:</strong> {{ $cliente->profissao }}</td>
            <td class="collumn-right"><strong>Observação:</strong> {{ $cliente->obsprofissao }}</td>
        </tr>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>Renda:</strong>
                {{ 'R$ ' . number_format($cliente->renda, 2, ',', '.') }}
            </td>
            <td class="collumn-right"></td>
        </tr>
    </tbody>
</table>

<br>
<h1 style="margin-left: 1%;">Endereço</h1>
<table>
    <tbody>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>CEP:</strong> {{ $cliente->cep }}</td>
            <td class="collumn-right"><strong>Rua/Av./Travessa:</strong> {{ $cliente->logradouro }}</td>
        </tr>
        <tr class="" style="font-size: 13px;">
            <td class="collumn-left"><strong>Nº:</strong> {{ $cliente->numero }}</td>
            <td class="collumn-right"><strong>Complemento:</strong> {{ $cliente->complemento }}</td>
        </tr>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>Bairro:</strong> {{ $cliente->bairro }}</td>
            <td class="collumn-right"><strong>Cidade</strong> {{ $cliente->cidade }}</td>
        </tr>
        <tr class="" style="font-size: 13px;">
            <td class="collumn-left"><strong>Estado:</strong> {{ $cliente->uf }}</td>
            <td class="collumn-right"></td>
        </tr>
    </tbody>
</table>

<br>
<h1 style="margin-left: 1%;">Dados Bancários</h1>
<table>
    <tbody>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>Banco:</strong> {{ $cliente->descricaobanco }}</td>
            <td class="collumn-right"><strong>Agência:</strong> {{ $cliente->agencia }}</td>
        </tr>
        <tr class="" style="font-size: 13px;">
            <td class="collumn-left"><strong>Conta:</strong> {{ $cliente->conta }}</td>
            <td class="collumn-right"></td>
        </tr>
    </tbody>
</table>

<br>
<h1 style="margin-left: 1%;">Informações Adicionais</h1>
<table>
    <tbody>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>Cadastro em Leiloeira:</strong>
                {{ $cliente->cadastroleiloeira == '1' ? 'SIM' : 'NÃO' }}</td>
            <td class="collumn-right"><strong>Leiloeira(s):</strong> {{ $cliente->leiloeira }}</td>
        </tr>
        <tr class="" style="font-size: 13px;">
            <td class="collumn-left"><strong>Estabelecimento:</strong> {{ $cliente->estabelecimento }}</td>
            <td class="collumn-right">
                <strong>Perfil: </strong>
                @switch($cliente->perfil)
                @case('C')
                COMPRA
                @break
                @case('V')
                VENDA
                @break
                @case('CV')
                COMPRA E VENDA
                @endswitch
            </td>
        </tr>
        <tr class="bg-light" style="font-size: 13px;">
            <td class="collumn-left"><strong>Observação:</strong> {{ $cliente->observacao }}</td>
            <td class="collumn-right"></td>
        </tr>
    </tbody>
</table>
@endsection

@section('footer')
@include('partials.reports.footer')
@endsection

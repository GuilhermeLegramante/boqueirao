<?php

namespace App\Http\Livewire\Documento;

use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use App\Services\CustomPaginator;
use Illuminate\Support\Facades\Session;
use App\Repositories\Cadastros\ClienteRepository;
use App\Repositories\Cadastros\DocumentoRepository;

class DocumentoComponent extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $cliente;
    public $tipodocumento;
    public $documento;

    private $documentoRepository;
    private $clienteRepository;
    public $sortBy = 'datainclusao';
    public $sortDirection = 'desc';
    protected $paginationTheme = 'bootstrap';
    public $perPage = 10;
    public $searchCliente = '';
    public $idt;

    protected $rules = [
        'documento' => 'required|max:10000',
    ];

    protected $validationAttributes = [
        'tipodocumento' => 'Tipo de Documento',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, [
            'documento' => 'required|max:10000',
        ]);
    }

    public function store()
    {
        $this->validate();
        $this->documentoRepository = new DocumentoRepository();

        $path =  time() . '.' . $this->documento->extension();

        $data = [
            'idtipodocumento' => $this->tipodocumento,
            'idcliente' => $this->cliente,
            'path' => $this->cliente . '/' . $path,
            'datainclusao' => date('Y-m-d H:i:s'),
            'idusuario' => Session::get('userId'),

        ];

        $create = $this->documentoRepository->create($data);

        if ($create == null) {
            session()->flash(
                'error',
                'Não foi possível completar a ação. Por favor, tente novamente.'
            );
            return redirect(route('documentos'));
        } else {

            $this->documento->storeAs($this->cliente, $path);

            $this->resetInputFields();

            session()->flash(
                'success',
                'Registro salvo com sucesso.'
            );
            return redirect(route('documentos'));
        }

        $this->emit('store');

    }

    public function resetInputFields()
    {
        $this->cliente = null;
        $this->tipodocumento = null;
        $this->documento = null;
    }

    public function delete($id)
    {
        $this->idt = $id;
        $this->documentoRepository = new DocumentoRepository();

        $data = $this->documentoRepository->findById($id);

    }

    public function destroy()
    {
        $this->documentoRepository = new DocumentoRepository();

        $delete = $this->documentoRepository->delete($this->idt);

        if ($delete) {
            session()->flash(
                'success',
                'Registro excluído com sucesso.'
            );
            $this->resetInputFields();
            return redirect(route('documentos'));

        } else {
            session()->flash(
                'error',
                'Não foi possível completar a ação. Por favor, tente novamente.'
            );
            return redirect(route('documentos'));
        }

        $this->emit('delete');
    }

    public function render()
    {
        $this->documentoRepository = new DocumentoRepository();
        $this->clienteRepository = new ClienteRepository();

        $clientes = $this->clienteRepository->getAll()->get();
        $tiposDocumento = $this->documentoRepository->getAllTiposDocumento();

        $documentos = $this->documentoRepository->getAll();

        if ($this->searchCliente != null) {
            $documentos = $documentos->where('cliente.nome', 'like', '%' . $this->searchCliente . '%');
        }

        $documentos = $documentos->orderBy($this->sortBy, $this->sortDirection)->paginate($this->perPage);

        return view('livewire.documento.component', compact('documentos', 'clientes', 'tiposDocumento'));
    }

    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        return $this->sortBy = $field;
    }

    public function cleanFilters()
    {
        $this->perPage = 10;
        $this->searchCliente = '';
    }

    public function load($value)
    {
        $this->perPage += $value;
    }
}

<?php

namespace App\Http\Livewire\Cadastros;

use App\Repositories\Cadastros\ClienteRepository;
use App\Services\CustomPaginator;
use Livewire\Component;
use Livewire\WithFileUploads;

class ClienteComponent extends Component
{
    use WithFileUploads;
    public $photo;

    public $idcliente;
    public $nome;
    public $email;
    public $cpfcnpj;
    public $rg;
    public $nomemae;
    public $nomepai;
    public $datanascimento;
    public $endereco;
    public $cep;
    public $logradouro;
    public $complemento;
    public $numero;
    public $bairro;
    public $cidade;
    public $uf;
    public $telefonecomercial;
    public $telefoneresidencial;
    public $telefonecelular;
    public $banco;
    public $agencia;
    public $conta;
    public $profissao;
    public $renda;
    public $leiloeira;
    public $habilitado = 0;
    public $cadastrocompleto = 0;
    public $estabelecimento;
    public $comprovantedocumento;
    public $comprovanteendereco;
    public $descricaoBanco;

    private $clienteRepository;
    public $sortBy = 'nome';
    public $sortDirection = 'asc';
    protected $paginationTheme = 'bootstrap';
    public $perPage = 1;
    public $searchNome = '';
    public $searchCpfCnpj = '';
    public $idt;
    public $updateMode = false;

    protected $rules = [
        'nome' => 'required|min:5|max:100',
        'cpfcnpj' => 'nullable|min:10',
        'nomemae' => 'nullable|min:5|max:100',
        'nomepai' => 'nullable|min:5|max:100',
        'email' => 'required|email',
        'rg' => 'nullable|min:5',
        'telefonecomercial' => 'numeric|nullable',
        'telefoneresidencial' => 'numeric|nullable',
        'telefonecelular' => 'numeric|nullable',
        'comprovantedocumento' => 'nullable|sometimes|max:10000',
        'comprovanteendereco' => 'nullable|sometimes|max:10000',
    ];

    protected $validationAttributes = [
        'cpfcnpj' => 'CPF ou CNPJ',
        'datanascimento' => 'data de nascimento',
        'telefonecomercial' => 'telefone comercial',
        'telefoneresidencial' => 'telefone residencial',
        'telefonecelular' => 'telefone celular',
        'comprovantedocumento' => 'comprovante do documento',
        'comprovanteendereco' => 'comprovante do endereço',
        'nomemae' => 'nome da mãe',
        'nomedopai' => 'nome do pai',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, [
            'cpfcnpj' => 'nullable|min:10',
            'rg' => 'nullable|min:5',
            'nome' => 'required|min:5|max:100',
            'nomemae' => 'nullable|min:5|max:100',
            'nomepai' => 'nullable|min:5|max:100',
            'email' => 'required|email',
            'telefonecomercial' => 'numeric|nullable',
            'telefoneresidencial' => 'numeric|nullable',
            'telefonecelular' => 'numeric|nullable',
            'comprovantedocumento' => 'nullable|sometimes|max:10000',
            'comprovanteendereco' => 'nullable|sometimes|max:10000',
        ]);
    }

    public function store()
    {
        $this->validate();
        $this->clienteRepository = new ClienteRepository();

        if ($this->cpfcnpj != '') {
            $this->cadastrocompleto = 1;
        }

        if (method_exists($this->comprovantedocumento, 'extension')) {
            $fileName_comprovantedocumento = 'comprovantedocumento' . '.' . $this->comprovantedocumento->extension();
        } else {
            $fileName_comprovantedocumento = '';
        }

        if (method_exists($this->comprovanteendereco, 'extension')) {
            $fileName_comprovanteendereco = 'comprovanteendereco' . '.' . $this->comprovanteendereco->extension();
        } else {
            $fileName_comprovanteendereco = '';
        }

        $data = [
            'nome' => strtoupper($this->nome),
            'nomemae' => strtoupper($this->nomemae),
            'nomepai' => strtoupper($this->nomepai),
            'email' => strtolower($this->email),
            'cpfcnpj' => $this->cpfcnpj,
            'rg' => $this->rg,
            'datanascimento' => $this->datanascimento,
            'cep' => $this->cep,
            'logradouro' => strtoupper($this->logradouro),
            'complemento' => strtoupper($this->complemento),
            'numero' => $this->numero,
            'bairro' => strtoupper($this->bairro),
            'cidade' => strtoupper($this->cidade),
            'uf' => strtoupper($this->uf),
            'telefonecomercial' => $this->telefonecomercial,
            'telefoneresidencial' => $this->telefoneresidencial,
            'telefonecelular' => $this->telefonecelular,
            'banco' => $this->banco,
            'agencia' => $this->agencia,
            'conta' => $this->conta,
            'profissao' => strtoupper($this->profissao),
            'renda' => $this->renda,
            'leiloeira' => $this->leiloeira,
            'habilitado' => $this->habilitado,
            'cadastrocompleto' => $this->cadastrocompleto,
            'estabelecimento' => $this->estabelecimento,
            'comprovantedocumento' => $fileName_comprovantedocumento,
            'comprovanteendereco' => $fileName_comprovanteendereco,

        ];

        $create = $this->clienteRepository->create($data);

        if ($create == null) {
            session()->flash(
                'error',
                'Não foi possível completar a ação. Por favor, tente novamente.'
            );
            return redirect(route('cadastros.cliente'));
        } else {
            if (method_exists($this->comprovantedocumento, 'extension')) {
                $this->comprovantedocumento->storeAs($create, $fileName_comprovantedocumento);
            }

            if (method_exists($this->comprovanteendereco, 'extension')) {
                $this->comprovanteendereco->storeAs($create, 'comprovanteendereco' . '.' . $fileName_comprovanteendereco);
            }

            $this->resetInputFields();
            session()->flash(
                'success',
                'Registro salvo com sucesso.'
            );
            return redirect(route('cadastros.cliente'));
        }

        $this->emit('store');

    }

    public function edit($id)
    {
        $this->updateMode = true;
        $this->idt = $id;
        $this->clienteRepository = new ClienteRepository();

        $data = $this->clienteRepository->findById($id);

        $this->idcliente = $data->idcliente;
        $this->nome = $data->nome;
        $this->email = $data->email;
        $this->cpfcnpj = $data->cpfcnpj;
        $this->rg = $data->rg;
        $this->nomemae = $data->nomemae;
        $this->nomepai = $data->nomepai;
        $this->datanascimento = $data->datanascimento;
        $this->endereco = $data->idendereco;
        $this->cep = $data->cep;
        $this->logradouro = $data->logradouro;
        $this->complemento = $data->complemento;
        $this->numero = $data->numero;
        $this->bairro = $data->bairro;
        $this->cidade = $data->cidade;
        $this->uf = $data->uf;
        $this->telefonecomercial = $data->telefonecomercial;
        $this->telefoneresidencial = $data->telefoneresidencial;
        $this->telefonecelular = $data->telefonecelular;
        $this->banco = $data->idbanco;
        $this->agencia = $data->agencia;
        $this->conta = $data->conta;
        $this->profissao = $data->profissao;
        $this->renda = $data->idrenda;
        $this->leiloeira = $data->idleiloeira;
        $this->habilitado = $data->habilitado;
        $this->cadastrocompleto = $data->cadastrocompleto;
        $this->estabelecimento = $data->idestabelecimento;
        $this->comprovantedocumento = $data->comprovantedocumento;
        $this->comprovanteendereco = $data->comprovanteendereco;
        $this->descricaoBanco = $data->descricaobanco;
        $this->descricaoEstabelecimento = $data->descricaoestabelecimento;
        $this->descricaoLeiloeira = $data->descricaoleiloeira;
        $this->descricaorenda = 'De ' . $data->valorinicial . ' a ' . $data->valorfinal . ' salários';

    }

    public function delete($id)
    {
        $this->idt = $id;
        $this->clienteRepository = new ClienteRepository();

        $data = $this->clienteRepository->findById($id);

        $this->nome = $data->nome;
        $this->idcliente = $data->idcliente;

    }

    public function destroy()
    {
        $this->clienteRepository = new ClienteRepository();

        $delete = $this->clienteRepository->delete($this->idt);

        if ($delete) {
            session()->flash(
                'success',
                'Registro excluído com sucesso.'
            );
            $this->resetInputFields();
            return redirect(route('cadastros.cliente'));

        } else {
            session()->flash(
                'error',
                'Não foi possível completar a ação. Por favor, tente novamente.'
            );
            return redirect(route('cadastros.cliente'));
        }

        $this->emit('delete');
    }

    public function update()
    {
        $this->validate();
        $this->clienteRepository = new ClienteRepository();

        if ($this->cpfcnpj != '') {
            $this->cadastrocompleto = 1;
        }

        if (method_exists($this->comprovantedocumento, 'extension')) {
            $fileName_comprovantedocumento = 'comprovantedocumento' . '.' . $this->comprovantedocumento->extension();
        } else {
            $fileName_comprovantedocumento = '';
        }

        if (method_exists($this->comprovanteendereco, 'extension')) {
            $fileName_comprovanteendereco = 'comprovanteendereco' . '.' . $this->comprovanteendereco->extension();
        } else {
            $fileName_comprovanteendereco = '';
        }

        $data = [
            'idcliente' => $this->idcliente,
            'nome' => strtoupper($this->nome),
            'nomemae' => strtoupper($this->nomemae),
            'nomepai' => strtoupper($this->nomepai),
            'email' => strtolower($this->email),
            'cpfcnpj' => $this->cpfcnpj,
            'rg' => $this->rg,
            'datanascimento' => $this->datanascimento,
            'endereco' => $this->endereco,
            'cep' => $this->cep,
            'logradouro' => strtoupper($this->logradouro),
            'complemento' => strtoupper($this->complemento),
            'numero' => $this->numero,
            'bairro' => strtoupper($this->bairro),
            'cidade' => strtoupper($this->cidade),
            'uf' => strtoupper($this->uf),
            'telefonecomercial' => $this->telefonecomercial,
            'telefoneresidencial' => $this->telefoneresidencial,
            'telefonecelular' => $this->telefonecelular,
            'banco' => $this->banco,
            'agencia' => $this->agencia,
            'conta' => $this->conta,
            'profissao' => strtoupper($this->profissao),
            'renda' => $this->renda,
            'leiloeira' => $this->leiloeira,
            'habilitado' => $this->habilitado,
            'cadastrocompleto' => $this->cadastrocompleto,
            'estabelecimento' => $this->estabelecimento,
            'comprovantedocumento' => $fileName_comprovantedocumento,
            'comprovanteendereco' => $fileName_comprovanteendereco,

        ];

        $update = $this->clienteRepository->update($data);


        if (($update == 0) || ($update == 1)) {
            if (method_exists($this->comprovantedocumento, 'extension')) {
                $this->comprovantedocumento->storeAs($data['idcliente'], $fileName_comprovantedocumento);
            }

            if (method_exists($this->comprovanteendereco, 'extension')) {
                $this->comprovanteendereco->storeAs($data['idcliente'], $fileName_comprovanteendereco);
            }

            $this->resetInputFields();
            session()->flash(
                'success',
                'Registro editado com sucesso.'
            );

            return redirect(route('cadastros.cliente'));

        } else {
            session()->flash(
                'error',
                'Não foi possível completar a ação. Por favor, tente novamente.'
            );
            return redirect(route('cadastros.cliente'));

        }

        $this->emit('store');
    }

    public function resetInputFields()
    {
        $this->nome = '';
        $this->email = '';
        $this->cpfcnpj = '';
        $this->rg = '';
        $this->nomemae = '';
        $this->nomepai = '';
        $this->datanascimento = '';
        $this->cep = '';
        $this->logradouro = '';
        $this->complemento = '';
        $this->numero = '';
        $this->bairro = '';
        $this->cidade = '';
        $this->uf = '';
        $this->telefonecomercial = '';
        $this->telefoneresidencial = '';
        $this->telefonecelular = '';
        $this->banco = '';
        $this->agencia = '';
        $this->conta = '';
        $this->profissao = '';
        $this->renda = '';
        $this->leiloeira = '';
        $this->habilitado = 0;
        $this->estabelecimento = '';
        $this->comprovantedocumento = '';
        $this->comprovanteendereco = '';
    }

    public function render()
    {
        $this->clienteRepository = new ClienteRepository();

        $estabelecimentos = $this->clienteRepository->getAllEstabelecimentos();
        $leiloeiras = $this->clienteRepository->getAllLeiloeiras();
        $faixasrenda = $this->clienteRepository->getAllFaixasRenda();
        $bancos = $this->clienteRepository->getAllBancos();

        if ((strlen($this->cpfcnpj) == 11) && (is_numeric($this->cpfcnpj))) {
            $this->cpfcnpj = substr($this->cpfcnpj, 0, 3) . '.' . substr($this->cpfcnpj, 3, 3) . '.' . substr($this->cpfcnpj, 6, 3) . '-' . substr($this->cpfcnpj, 9);
        }

        if ((strlen($this->cpfcnpj) >= 14) && (is_numeric($this->cpfcnpj))) {
            $this->cpfcnpj = substr($this->cpfcnpj, 0, 2) . '.' . substr($this->cpfcnpj, 2, 3) . '.' . substr($this->cpfcnpj, 5, 3) . '/' . substr($this->cpfcnpj, 8, 4) . '-' . substr($this->cpfcnpj, 12, 2);
        }

        $data = $this->clienteRepository->getAll();

        $this->searchNome = strtoupper($this->searchNome);
        $this->searchCpfCnpj = strtoupper($this->searchCpfCnpj);

        if ($this->sortDirection == 'asc') {
            $clientes = $data->sortBy($this->sortBy);
        } else {
            $clientes = $data->sortByDesc($this->sortBy);
        }

        if ($this->searchNome != '') {
            $searchNome = strtoupper($this->searchNome);

            $clientes = $clientes->reject(function ($item) use ($searchNome) {
                return mb_strpos($item->nome, $searchNome) === false;
            });
        }

        if ($this->searchCpfCnpj != '') {
            $searchCpfCnpj = strtoupper($this->searchCpfCnpj);

            $clientes = $clientes->reject(function ($item) use ($searchCpfCnpj) {
                return mb_strpos($item->cpfcnpj, $searchCpfCnpj) === false;
            });
        }

        $clientes = CustomPaginator::paginate($clientes, $this->perPage, null, ['path' => env('PAGINATE_URL_CLIENTES')]);

        return view('livewire.cadastros.cliente-component', compact('clientes', 'estabelecimentos', 'leiloeiras', 'faixasrenda', 'bancos'));
    }

    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        return $this->sortBy = $field;
    }

    public function cleanFilters()
    {
        $this->perPage = 1;
        $this->searchNome = '';
        $this->searchCpfCnpj = '';
    }

    public function load($value)
    {
        $this->perPage += $value;
    }
}

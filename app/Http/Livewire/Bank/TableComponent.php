<?php

namespace App\Http\Livewire\Bank;

use Livewire\Component;
use App\Traits\WithReport;
use Livewire\WithPagination;
use App\Traits\WithDatatable;
use Illuminate\Support\Facades\App;
use App\Repositories\BankRepository;

class TableComponent extends Component
{
    use WithPagination, WithDatatable, WithReport;

    public function showForm($id = null)
    {
        $this->emit('modalBank', $id);
    }

    public function render()
    {
        $repository = App::make(BankRepository::class);

        $data = $repository->all($this->search, $this->sortBy, $this->sortDirection, $this->perPage);

        return view('livewire.bank.table-component', compact('data'));
    }
}

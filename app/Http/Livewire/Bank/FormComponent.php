<?php

namespace App\Http\Livewire\Bank;

use App\Services\Mask;
use Livewire\Component;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Repositories\BankRepository;

class FormComponent extends Component
{
    public $code;
    public $description;
    public $recordId;
    public $isEdition;
    public $redirect;
    public $basePath = 'bank';

    protected $listeners = [
        'modalBank',
    ];

    public function modalBank($redirect, $id = null)
    {
        $this->redirect = $redirect;
        $this->recordId = $id;
        $repo = new BankRepository();

        if (($id == '') || ($id == null)) {
            $this->recordId = null;
            $this->isEdition = false;
            $this->unsetFields();
        } else {
            $this->recordId = $id;
            $this->isEdition = true;
            $data = $repo->findById($id);
            $this->setFields($data);
        }

        $this->emit('showModalBank');
    }

    public function rules()
    {
        return [
            'code' => [
                'required',
                'numeric',
                $this->isEdition == true ?
                Rule::unique('banco', 'cod')->ignore($this->recordId)
                :
                Rule::unique('banco', 'cod'),
            ],
            'description' => 'required|max:100',
        ];
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, $this->rules());
    }

    protected $validationAttributes = [
        'code' => 'Código',
        'description' => 'Descrição',
    ];

    public function setFields($data)
    {
        $this->code = $data->code;
        $this->description = $data->description;
    }

    public function unsetFields()
    {
        $this->code = '';
        $this->description = '';
    }

    public function showModalDelete()
    {
        $this->emit('delete');
    }

    public function render()
    {
        return view('livewire.bank.form-component');
    }

    public function store()
    {
        $this->validate();

        DB::beginTransaction();

        try
        {
            $repository = new BankRepository();

            $data = [
                'code' => $this->code,
                'description' => Mask::normalizeString($this->description),
            ];

            $repository->save($data);

            session()->flash('success', 'Registro salvo com sucesso');
            DB::commit();

            return redirect()->route($this->basePath);
        } catch (\Exception $error) {
            DB::rollback();
            // isset($error->errorInfo) && $error->errorInfo[0] == '23000' ? session()->flash('error', config('messages.mysql.' . $error->errorInfo[1])) :
            session()->flash('error', $error->getMessage());
        }
    }

    public function update()
    {
        $this->validate();

        DB::beginTransaction();

        try {
            $repository = new BankRepository();

            $data = [
                'recordId' => $this->recordId,
                'code' => $this->code,
                'description' => Mask::normalizeString($this->description),
            ];

            $repository->update($data);

            session()->flash('success', 'Registro editado com sucesso');
            DB::commit();

            return redirect()->route($this->basePath);
        } catch (\Exception $error) {
            DB::rollback();
            // isset($error->errorInfo) && $error->errorInfo[0] == '23000' ? session()->flash('error', config('messages.mysql.' . $error->errorInfo[1])) :
            session()->flash('error', $error->getMessage());
        }
    }

    public function destroy()
    {
        DB::beginTransaction();

        try {
            $repository = App::make(BankRepository::class);
            $repository->delete([
                'recordId' => $this->recordId,
            ]);
            session()->flash('success', 'Registro excluído com sucesso');
            DB::commit();

            return redirect()->route($this->basePath);
        } catch (\Exception $error) {
            DB::rollback();

            $this->emit('close');

            // isset($error->errorInfo) && $error->errorInfo[0] == '23000' ? session()->flash('error', config('messages.mysql.' . $error->errorInfo[1])) :
            session()->flash('error', $error->getMessage());
        }
    }
}

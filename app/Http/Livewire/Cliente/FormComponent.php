<?php

namespace App\Http\Livewire\Cliente;

use App\Services\Mask;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Session;
use App\Repositories\Cadastros\ClienteRepository;
use App\Repositories\Cadastros\DocumentoRepository;
use App\Repositories\TipoDocumentoRepository;
use App\Services\ViaCep;

class FormComponent extends Component
{
    use WithFileUploads;

    private $clienteRepository;
    private $documentoRepository;

    public $idcliente;
    public $canalinclusao = 'DIVULGACAO';
    public $nome;
    public $email;
    public $cpfcnpj;
    public $rg;
    public $nomemae;
    public $nomepai;
    public $datanascimento;
    public $endereco;
    public $cep;
    public $logradouro;
    public $complemento;
    public $numero;
    public $bairro;
    public $cidade;
    public $uf;
    public $telefonecomercial;
    public $telefoneresidencial;
    public $telefonecelular;
    public $celularwhats;
    public $banco;
    public $agencia;
    public $conta;
    public $profissao;
    public $obsprofissao;
    public $renda;
    public $cadastroleiloeira = 0;
    public $leiloeira;
    public $situacao = 'INABILITADO';
    public $estabelecimento;


    public $documentoPessoal;
    public $comprovanteRenda;
    public $comprovanteResidencia;
    public $consultaFinanceira;

    public $observacao;

    public $perfil;

    public $tiposDocumento = [];

    public $idTipoDocumento;

    public $arquivo;

    public $documentos = [];

    protected $rules = [
        'nome' => 'required|min:5|max:100',
        'cpfcnpj' => 'nullable|min:10',
        'nomemae' => 'nullable|min:5|max:100',
        'nomepai' => 'nullable|min:5|max:100',
        'email' => 'required|email',
        'rg' => 'nullable|min:5',
        'estabelecimento' => 'nullable|min:5|max:100',
    ];

    protected $validationAttributes = [
        'cpfcnpj' => 'CPF ou CNPJ',
        'datanascimento' => 'data de nascimento',
        'comprovantedocumento' => 'comprovante do documento',
        'comprovanteendereco' => 'comprovante do endereço',
        'nomemae' => 'nome da mãe',
        'nomedopai' => 'nome do pai',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, [
            'nome' => 'required|min:5|max:100',
            'cpfcnpj' => 'nullable|min:10',
            'nomemae' => 'nullable|min:5|max:100',
            'nomepai' => 'nullable|min:5|max:100',
            'email' => 'required|email',
            'rg' => 'nullable|min:5',
            'estabelecimento' => 'nullable|min:5|max:100',
        ]);
    }

    public function updatedRenda()
    {
        $this->renda = Mask::money($this->renda);
    }

    public function updatedCep()
    {
        if (is_numeric($this->cep)) {
            $this->cep = Mask::cep($this->cep);
        }

        $data = ViaCep::search($this->cep);

        if (isset($data)) {
            if (!isset($data['erro'])) {
                $this->cep = Mask::cep($data['cep']);
                $this->logradouro = $data['logradouro'];
                $this->bairro = $data['bairro'];
                $this->cidade = $data['localidade'];
                $this->uf = $data['uf'];
            } else {
                session()->flash('cepError', 'CEP inexistente.');
            }
        }
    }

    public function mount()
    {
        $repository = new TipoDocumentoRepository();

        $this->tiposDocumento = json_decode(json_encode($repository->all()->get()), true);
    }

    public function store()
    {
        $this->validate();
        $this->clienteRepository = new ClienteRepository();
        $this->documentoRepository = new DocumentoRepository();

        $data = [
            'canalinclusao' => $this->canalinclusao,
            'nome' => Mask::normalizeString($this->nome),
            'email' => strtolower($this->email),
            'datanascimento' => $this->datanascimento,
            'cpfcnpj' => $this->cpfcnpj,
            'rg' => $this->rg,
            'telefonecomercial' => $this->telefonecomercial,
            'telefoneresidencial' => $this->telefoneresidencial,
            'telefonecelular' => $this->telefonecelular,
            'celularwhats' => $this->celularwhats,
            'nomemae' => Mask::normalizeString($this->nomemae),
            'nomepai' => Mask::normalizeString($this->nomepai),
            'cep' => $this->cep,
            'logradouro' => Mask::normalizeString($this->logradouro),
            'numero' => $this->numero,
            'complemento' => Mask::normalizeString($this->complemento),
            'bairro' => Mask::normalizeString($this->bairro),
            'cidade' => Mask::normalizeString($this->cidade),
            'uf' => Mask::normalizeString($this->uf),
            'profissao' => Mask::normalizeString($this->profissao),
            'obsprofissao' => Mask::normalizeString($this->obsprofissao),
            'renda' => Mask::removeMoneyMask($this->renda),
            'banco' => $this->banco,
            'agencia' => $this->agencia,
            'conta' => $this->conta,
            'cadastroleiloeira' => $this->cadastroleiloeira,
            'leiloeira' => $this->leiloeira,
            'estabelecimento' => $this->estabelecimento,
            'situacao' => $this->situacao,
            'idusuario' => Session::get('userId'),
            'datacadastro' => date('Y-m-d H:i:s'),
            'dataalteracao' => date('Y-m-d H:i:s'),
            'observacao' => Mask::normalizeString($this->observacao),
            'perfil' => $this->perfil,
        ];


        $idCliente = $this->clienteRepository->create($data);

        if ($idCliente == null) {
            session()->flash(
                'error',
                'Não foi possível completar a ação. Por favor, tente novamente.'
            );
            return redirect(route('clientes'));
        } else {
            if (count($this->documentos) > 0) {
                foreach ($this->documentos as $key => $documento) {
                    if ($documento['arquivo'] != null) {
                        $path = uniqid() . '.' . $documento['arquivo']->extension();

                        $documento['arquivo']->storeAs($idCliente, $path);

                        $repository = new DocumentoRepository();

                        $repository->create(
                            [
                                'idtipodocumento' => $documento['idTipoDocumento'],
                                'idcliente' => $idCliente,
                                'idusuario' => Session::get('userId'),
                                'path' => $idCliente . '/' . $path,
                                'datainclusao' => date('Y-m-d H:i:s'),
                            ]
                        );
                    }
                }
            }

            $this->resetInputFields();
            session()->flash(
                'success',
                'Registro salvo com sucesso.'
            );
            return redirect(route('clientes'));
        }
    }

    public function storeDocumento()
    {
        $this->validate([
            'idTipoDocumento' => 'required',
            'arquivo' => 'required',
        ], [
            'idTipoDocumento.required' => 'O campo tipo de documento é obrigatório',
        ]);

        $repository = new TipoDocumentoRepository();

        $tipoDocumento = $repository->findById($this->idTipoDocumento);

        $documento = [];

        $documento['idTipoDocumento'] = $tipoDocumento->id;

        $documento['descricaoTipoDocumento'] = $tipoDocumento->descricao;

        $documento['arquivo'] = $this->arquivo;

        $documento['nomeArquivo'] = $this->arquivo->getClientOriginalName();

        array_push($this->documentos, $documento);

        $this->reset('idTipoDocumento');

        $this->reset('arquivo');
    }

    public function deleteDocumento($key)
    {
        unset($this->documentos[$key]);
    }

    public function render()
    {
        $this->clienteRepository = new ClienteRepository();
        $bancos = $this->clienteRepository->getAllBancos();

        $this->cpfcnpj = Mask::cpfCnpj($this->cpfcnpj);
        $this->celularwhats = Mask::celular($this->celularwhats);
        $this->telefonecelular = Mask::celular($this->telefonecelular);
        $this->telefonecomercial = Mask::telFixo($this->telefonecomercial);
        $this->telefoneresidencial = Mask::telFixo($this->telefoneresidencial);

        return view('livewire.cliente.form-component', compact('bancos'));
    }

    public function resetInputFields()
    {
        $this->canalinclusao = '';
        $this->nome = '';
        $this->email = '';
        $this->cpfcnpj = '';
        $this->rg = '';
        $this->nomemae = '';
        $this->nomepai = '';
        $this->datanascimento = '';
        $this->cep = '';
        $this->logradouro = '';
        $this->complemento = '';
        $this->numero = '';
        $this->bairro = '';
        $this->cidade = '';
        $this->uf = '';
        $this->telefonecomercial = '';
        $this->telefoneresidencial = '';
        $this->telefonecelular = '';
        $this->banco = '';
        $this->agencia = '';
        $this->conta = '';
        $this->profissao = '';
        $this->renda = '';
        $this->cadastroleiloeira = '';
        $this->leiloeira = '';
        $this->situacao = 'HABILITADO';
        $this->estabelecimento = '';
        $this->observacao = '';
    }
}

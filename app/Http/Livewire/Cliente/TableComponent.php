<?php

namespace App\Http\Livewire\Cliente;

use App\Repositories\Cadastros\ClienteRepository;
use Livewire\Component;
use Livewire\WithPagination;

class TableComponent extends Component
{
    use WithPagination;

    private $clienteRepository;
    public $sortBy = 'nome';
    public $sortDirection = 'asc';
    protected $paginationTheme = 'bootstrap';
    public $perPage = 10;
    public $search = '';

    public function render()
    {
        $this->clienteRepository = new ClienteRepository();

        $clientes = $this->clienteRepository->getAll();

        if ($this->search != null) {
            $clientes = $clientes
                ->where('cliente.nome', 'like', '%' . $this->search . '%')
                ->orWhere('cliente.cpfcnpj', 'like', '%' . $this->search . '%');
        }

        $clientes = $clientes->orderBy($this->sortBy, $this->sortDirection)->paginate($this->perPage);

        return view('livewire.cliente.table-component', compact('clientes'));
    }

    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        return $this->sortBy = $field;
    }

    public function cleanFilters()
    {
        $this->perPage = 10;
        $this->search = '';
    }

    public function load($value)
    {
        $this->perPage += $value;
    }
}

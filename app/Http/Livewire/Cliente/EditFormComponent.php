<?php

namespace App\Http\Livewire\Cliente;

use App\Repositories\Cadastros\ClienteRepository;
use App\Repositories\Cadastros\DocumentoRepository;
use App\Repositories\TipoDocumentoRepository;
use App\Services\Mask;
use App\Services\ViaCep;
use Illuminate\Support\Facades\Session;
use Livewire\Component;
use Livewire\WithFileUploads;

class EditFormComponent extends Component
{
    use WithFileUploads;

    private $clienteRepository;
    private $documentoRepository;

    public $idcliente;
    public $canalinclusao = 'DIVULGACAO';
    public $nome;
    public $email;
    public $cpfcnpj;
    public $rg;
    public $nomemae;
    public $nomepai;
    public $datanascimento;
    public $endereco;
    public $cep;
    public $logradouro;
    public $complemento;
    public $numero;
    public $bairro;
    public $cidade;
    public $uf;
    public $telefonecomercial;
    public $telefoneresidencial;
    public $telefonecelular;
    public $celularwhats;
    public $banco;
    public $descricaobanco;
    public $agencia;
    public $conta;
    public $profissao;
    public $obsprofissao;
    public $renda;
    public $cadastroleiloeira;
    public $leiloeira;
    public $situacao = 'INABILITADO';
    public $estabelecimento;

    public $old_canalinclusao;
    public $old_uf;
    public $old_cadastroleiloeira;
    public $old_situacao;

    private $cliente;
    public $documentoPessoal;
    public $comprovanteRenda;
    public $comprovanteResidencia;
    public $consultaFinanceira;

    public $observacao;

    public $perfil;

    public $updateMode = false;

    public $tiposDocumento = [];

    public $idTipoDocumento;

    public $arquivo;

    public $documentos = [];

    public $storedDocumentos = [];

    protected $rules = [
        'nome' => 'required|min:5|max:100',
        'cpfcnpj' => 'nullable|min:10',
        'nomemae' => 'nullable|min:5|max:100',
        'nomepai' => 'nullable|min:5|max:100',
        'email' => 'required|email',
        'rg' => 'nullable|min:5',
        'estabelecimento' => 'nullable|min:5|max:100',
    ];

    protected $validationAttributes = [
        'cpfcnpj' => 'CPF ou CNPJ',
        'datanascimento' => 'data de nascimento',
        'comprovantedocumento' => 'comprovante do documento',
        'comprovanteendereco' => 'comprovante do endereço',
        'nomemae' => 'nome da mãe',
        'nomedopai' => 'nome do pai',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, [
            'nome' => 'required|min:5|max:100',
            'cpfcnpj' => 'nullable|min:10',
            'nomemae' => 'nullable|min:5|max:100',
            'nomepai' => 'nullable|min:5|max:100',
            'email' => 'required|email',
            'rg' => 'nullable|min:5',
            'estabelecimento' => 'nullable|min:5|max:100',
        ]);
    }

    public function updatedRenda()
    {
        $this->renda = Mask::money($this->renda);
    }

    public function updatedCep()
    {
        if (is_numeric($this->cep)) {
            $this->cep = Mask::cep($this->cep);
        }

        $data = ViaCep::search($this->cep);

        if (isset($data)) {
            if (!isset($data['erro'])) {
                $this->cep = Mask::cep($data['cep']);
                $this->logradouro = $data['logradouro'];
                $this->bairro = $data['bairro'];
                $this->cidade = $data['localidade'];
                $this->uf = $data['uf'];
            } else {
                session()->flash('cepError', 'CEP inexistente.');
            }
        }
    }

    public function update()
    {
        $this->validate();
        $this->clienteRepository = new ClienteRepository();
        $this->documentoRepository = new DocumentoRepository();

        $data = [
            'idcliente' => $this->idcliente,
            'canalinclusao' => $this->canalinclusao,
            'nome' => Mask::normalizeString($this->nome),
            'email' => strtolower($this->email),
            'datanascimento' => $this->datanascimento,
            'cpfcnpj' => $this->cpfcnpj,
            'rg' => $this->rg,
            'telefonecomercial' => $this->telefonecomercial,
            'telefoneresidencial' => $this->telefoneresidencial,
            'telefonecelular' => $this->telefonecelular,
            'celularwhats' => $this->celularwhats,
            'nomemae' => Mask::normalizeString($this->nomemae),
            'nomepai' => Mask::normalizeString($this->nomepai),
            'endereco' => $this->endereco,
            'cep' => $this->cep,
            'logradouro' => Mask::normalizeString($this->logradouro),
            'numero' => $this->numero,
            'complemento' => Mask::normalizeString($this->complemento),
            'bairro' => Mask::normalizeString($this->bairro),
            'cidade' => Mask::normalizeString($this->cidade),
            'uf' => Mask::normalizeString($this->uf),
            'profissao' => Mask::normalizeString($this->profissao),
            'obsprofissao' => Mask::normalizeString($this->obsprofissao),
            'renda' => Mask::removeMoneyMask($this->renda),
            'banco' => $this->banco,
            'agencia' => $this->agencia,
            'conta' => $this->conta,
            'cadastroleiloeira' => $this->cadastroleiloeira,
            'leiloeira' => $this->leiloeira,
            'estabelecimento' => $this->estabelecimento,
            'situacao' => $this->situacao,
            'idusuario' => Session::get('userId'),
            'dataalteracao' => date('Y-m-d H:i:s'),
            'observacao' => Mask::normalizeString($this->observacao),
            'perfil' => $this->perfil,
        ];

        $update = $this->clienteRepository->update($data);

        if ($update == null) {
            session()->flash(
                'error',
                'Não foi possível completar a ação. Por favor, tente novamente.'
            );
            return redirect(route('clientes'));
        } else {
            if (count($this->documentos) > 0) {
                foreach ($this->documentos as $key => $documento) {
                    if ($documento['arquivo'] != null) {
                        $path = uniqid() . '.' . $documento['arquivo']->extension();

                        $documento['arquivo']->storeAs($this->idcliente, $path);

                        $repository = new DocumentoRepository();

                        $repository->create(
                            [
                                'idtipodocumento' => $documento['idTipoDocumento'],
                                'idcliente' => $this->idcliente,
                                'idusuario' => Session::get('userId'),
                                'path' => $this->idcliente . '/' . $path,
                                'datainclusao' => date('Y-m-d H:i:s'),
                            ]
                        );
                    }
                }
            }

            $this->resetInputFields();
            session()->flash(
                'success',
                'Registro salvo com sucesso.'
            );
            return redirect(route('clientes'));
        }
    }

    public function destroy()
    {
        $this->clienteRepository = new ClienteRepository();

        $delete = $this->clienteRepository->delete($this->idcliente);

        if ($delete) {
            session()->flash(
                'success',
                'Registro excluído com sucesso.'
            );
            $this->resetInputFields();
            return redirect(route('clientes'));
        } else {
            session()->flash(
                'error',
                'Não foi possível completar a ação. Por favor, tente novamente.'
            );
            return redirect(route('clientes'));
        }

        $this->emit('delete');
    }

    public function mount($idcliente)
    {
        $this->idcliente = $idcliente;
        $this->clienteRepository = new ClienteRepository();
        $this->cliente = $this->clienteRepository->findById($this->idcliente);
        $this->setValues($this->cliente);

        $repository = new TipoDocumentoRepository();

        $this->tiposDocumento = json_decode(json_encode($repository->all()->get()), true);

        $repository = new ClienteRepository();

        $this->storedDocumentos = json_decode(json_encode($repository->documentos($idcliente)), true);
    }

    public function render()
    {
        $this->clienteRepository = new ClienteRepository();
        $this->documentoRepository = new DocumentoRepository();

        $bancos = $this->clienteRepository->getAllBancos();

        $this->cpfcnpj = Mask::cpfCnpj($this->cpfcnpj);
        $this->celularwhats = Mask::celular($this->celularwhats);
        $this->telefonecelular = Mask::celular($this->telefonecelular);
        $this->telefonecomercial = Mask::telFixo($this->telefonecomercial);
        $this->telefoneresidencial = Mask::telFixo($this->telefoneresidencial);

        return view('livewire.cliente.edit-form-component', compact('bancos'));
    }

    public function storeDocumento()
    {
        $this->validate([
            'idTipoDocumento' => 'required',
            'arquivo' => 'required',
        ], [
            'idTipoDocumento.required' => 'O campo tipo de documento é obrigatório',
        ]);

        $repository = new TipoDocumentoRepository();

        $tipoDocumento = $repository->findById($this->idTipoDocumento);

        $documento = [];

        $documento['idTipoDocumento'] = $tipoDocumento->id;

        $documento['descricaoTipoDocumento'] = $tipoDocumento->descricao;

        $documento['arquivo'] = $this->arquivo;

        $documento['nomeArquivo'] = $this->arquivo->getClientOriginalName();

        array_push($this->documentos, $documento);

        $this->reset('idTipoDocumento');

        $this->reset('arquivo');
    }

    public function deleteDocumento($key)
    {
        unset($this->documentos[$key]);
    }

    public function setValues($cliente)
    {
        $this->canalinclusao = $cliente->canalinclusao;
        $this->nome = $cliente->nome;
        $this->email = $cliente->email;
        $this->cpfcnpj = $cliente->cpfcnpj;
        $this->rg = $cliente->rg;
        $this->nomemae = $cliente->nomemae;
        $this->nomepai = $cliente->nomepai;
        $this->datanascimento = $cliente->datanascimento;
        $this->endereco = $cliente->endereco;
        $this->cep = $cliente->cep;
        $this->logradouro = $cliente->logradouro;
        $this->complemento = $cliente->complemento;
        $this->numero = $cliente->numero;
        $this->bairro = $cliente->bairro;
        $this->cidade = $cliente->cidade;
        $this->uf = $cliente->uf;
        $this->telefonecomercial = $cliente->telefonecomercial;
        $this->telefoneresidencial = $cliente->telefoneresidencial;
        $this->telefonecelular = $cliente->telefonecelular;
        $this->celularwhats = $cliente->celularwhats;
        $this->banco = $cliente->idbanco;
        $this->descricaobanco = $cliente->descricaobanco;
        $this->agencia = $cliente->agencia;
        $this->conta = $cliente->conta;
        $this->profissao = $cliente->profissao;
        $this->renda = Mask::money($cliente->renda);
        $this->cadastroleiloeira = $cliente->cadastroleiloeira;
        $this->leiloeira = $cliente->leiloeira;
        $this->situacao = $cliente->situacao;
        $this->estabelecimento = $cliente->estabelecimento;
        $this->observacao = $cliente->observacao;
        $this->perfil = $cliente->perfil;

        $this->old_canalinclusao = $cliente->canalinclusao;
        $this->old_uf = $cliente->uf;
        $this->old_cadastroleiloeira = $cliente->cadastroleiloeira;
        $this->old_situacao = $cliente->situacao;
    }

    public function resetInputFields()
    {
        $this->canalinclusao = '';
        $this->nome = '';
        $this->email = '';
        $this->cpfcnpj = '';
        $this->rg = '';
        $this->nomemae = '';
        $this->nomepai = '';
        $this->datanascimento = '';
        $this->cep = '';
        $this->logradouro = '';
        $this->complemento = '';
        $this->numero = '';
        $this->bairro = '';
        $this->cidade = '';
        $this->uf = '';
        $this->telefonecomercial = '';
        $this->telefoneresidencial = '';
        $this->telefonecelular = '';
        $this->celularwhats = '';
        $this->banco = '';
        $this->agencia = '';
        $this->conta = '';
        $this->profissao = '';
        $this->renda = '';
        $this->cadastroleiloeira = '';
        $this->leiloeira = '';
        $this->situacao = 'HABILITADO';
        $this->estabelecimento = '';
        $this->observacao = '';
    }
}

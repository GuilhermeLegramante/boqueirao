<?php

namespace App\Http\Livewire\TipoDocumento;

use App\Repositories\TipoDocumentoRepository;
use App\Services\Mask;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class FormComponent extends Component
{
    public $descricao;
    public $recordId;
    public $isEdition;
    public $redirect;
    public $basePath = 'tipo-documento';

    protected $listeners = [
        'modalTipoDocumento',
    ];

    public function modalTipoDocumento($redirect, $id = null)
    {
        $this->redirect = $redirect;
        $this->recordId = $id;
        $repo = new TipoDocumentoRepository();

        if (($id == '') || ($id == null)) {
            $this->recordId = null;
            $this->isEdition = false;
            $this->unsetFields();
        } else {
            $this->recordId = $id;
            $this->isEdition = true;
            $data = $repo->findById($id);
            $this->setFields($data);
        }

        $this->emit('showModalTipoDocumento');
    }

    public function rules()
    {
        return [
            'descricao' => 'required|max:100',
        ];
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName, $this->rules());
    }

    protected $validationAttributes = [
        'descricao' => 'Descrição',
    ];

    public function setFields($data)
    {
        $this->descricao = $data->descricao;
    }

    public function unsetFields()
    {
        $this->descricao = '';
    }

    public function showModalDelete()
    {
        $this->emit('delete');
    }

    public function render()
    {
        return view('livewire.tipo-documento.form-component');
    }

    public function store()
    {
        $this->validate();

        DB::beginTransaction();

        try
        {
            $repository = new TipoDocumentoRepository();

            $data = [
                'descricao' => Mask::normalizeString($this->descricao),
            ];

            $save = $repository->save($data);

            if ($save['success'] == true) {
                DB::commit();
                if ($this->redirect != 'false') {
                    return redirect()->route($this->basePath);
                } else {
                    $this->emit('closeModalTipoDocumento');
                    // $this->emit('messageFromParam', config('messages.insert.success'));
                }
            } else {
                DB::rollback();
                session()->flash('error', $save['content']);
                return redirect()->route($this->basePath);
            }
        } catch (\Exception $error) {
            DB::rollback();
            session()->flash('error', $error->getMessage());
            return redirect()->route($this->basePath);
        }
    }

    public function update()
    {
        $this->validate();

        DB::beginTransaction();

        try {
            $repository = new TipoDocumentoRepository();

            $data = [
                'id' => $this->recordId,
                'descricao' => Mask::normalizeString($this->descricao),
            ];

            $update = $repository->update($data);

            if ($update['success'] == true) {
                DB::commit();
                session()->flash('success', config('messages.edit.success'));
                $this->emit('closeModalTipoDocumento');
                return redirect()->route($this->basePath);
            } else {
                DB::rollback();
                session()->flash('error', $update['content']);
                return redirect()->route($this->basePath);
            }
        } catch (\Exception $error) {
            DB::rollback();
            session()->flash('error', $error->getMessage());
            return redirect()->route($this->basePath);
        }
    }

    public function destroy()
    {
        $data = [
            'id' => $this->recordId,
        ];

        DB::beginTransaction();

        try {
            $repository = new TipoDocumentoRepository();
            $delete = $repository->delete($data);

            if ($delete['success']) {
                DB::commit();
                session()->flash('success', config('messages.delete.success'));
                return redirect()->route($this->basePath);
            } else {
                DB::rollback();
                session()->flash('error', $delete['content']);
                return redirect()->route($this->basePath);
            }
        } catch (\Exception $error) {
            DB::rollback();
            session()->flash('error', $error->getMessage());
            return redirect()->route($this->basePath);
        }
    }
}

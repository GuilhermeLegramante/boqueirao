<?php

namespace App\Http\Livewire\TipoDocumento;

use Livewire\Component;
use App\Traits\WithReport;
use Livewire\WithPagination;
use App\Traits\WithDatatable;
use App\Repositories\TipoDocumentoRepository;

class TableComponent extends Component
{
    use WithPagination;
    use WithDatatable;
    use WithReport;

    public function showForm($id = null)
    {
        $this->emit('modalTipoDocumento', $id);
    }

    public function render()
    {
        $repo = new TipoDocumentoRepository();

        $data = $repo->all();

        if ($this->searchDesc != null) {
            $data = $data->where('tipodocumento' . '.descricao', 'like', '%' . $this->searchDesc . '%');
        }

        if ($this->searchCod != null) {
            $data = $data->where('tipodocumento' . '.id', 'like', '%' . $this->searchCod . '%');
        }

        $data = $data->orderBy($this->sortBy, $this->sortDirection)->paginate($this->perPage);

        if ($data->total() == $data->lastItem()) {
            $this->emit('scrollTop');
        }

        return view('livewire.tipo-documento.table-component', compact('data'));
    }
}

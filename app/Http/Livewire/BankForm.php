<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\App;
use App\Http\Livewire\Traits\WithForm;
use Illuminate\Validation\Rule;

class BankForm extends Component
{
    use WithForm;

    public $pageTitle = 'Banco';
    public $icon = 'fas fa-building';
    public $basePath = 'bank.table';
    public $previousRoute = 'bank.table';
    public $method = 'store';

    protected $repositoryClass = 'App\Repositories\BankRepository';

    public $description;

    public $code;

    protected $inputs = [
        ['field' => 'recordId', 'edit' => true],
        ['field' => 'description', 'edit' => true, 'type' => 'string'],
        ['field' => 'code', 'edit' => true, 'type' => 'string'],
    ];

    protected $validationAttributes = [
        'description' => 'Descrição',
        'code' => 'Código',
    ];

    public function rules()
    {
        return [
            'description' => ['required', 'max:100'],
            'code' => [
                'required',
                $this->isEdition == true ? Rule::unique('banco', 'cod')->ignore($this->recordId) : Rule::unique('banco', 'cod'),
                'numeric'
            ],
        ];
    }

    public function mount($id = null)
    {
        if (isset($id)) {
            $this->method = 'update';

            $this->isEdition = true;

            $repository = App::make($this->repositoryClass);

            $data = $repository->findById($id);

            if (isset($data)) {
                $this->setFields($data);
            }
        }
    }

    public function setFields($data)
    {
        $this->recordId = $data->id;

        $this->description = $data->description;

        $this->code = $data->code;
    }

    public function customValidate()
    {
        return true;
    }

    public function customDeleteValidate()
    {
        return true;
    }

    public function render()
    {
        return view('livewire.bank-form');
    }
}

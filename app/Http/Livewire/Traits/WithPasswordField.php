<?php

namespace App\Http\Livewire\Traits;

use App\Services\Mask;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;

trait WithPasswordField
{

    public function showHideText()
    {
        if($this->showText){
            $this->showText = false;
        } else {
            $this->showText = true;
        }
    }
}

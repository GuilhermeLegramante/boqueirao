<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Repositories\Cadastros\UserRepository;

class LoginController extends Controller
{
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    public function loginView()
    {
        $this->setInitialSessions();
        return view('auth.login');
    }

    public function resetPasswordView()
    {
        $this->setInitialSessions();
        return view('auth.reset');
    }

    public function login(LoginRequest $request)
    {
        $login = $request->login;
        $password = sha1($request->senha);

        $user = $this->getUserByAuth($login, $password);

        if ($user == null) {
            return redirect()->back()->with('error', 'Dados incorretos.');
        } else {
            $this->setUserSessions($user);
            $this->setInitialSessions();
            return redirect()->route('dashboard');
        }
    }

    public function setUserSessions($user)
    {
        Session::put('login', true);
        Session::put('name', $user->nome);
        Session::put('userId', $user->id);
    }

    public function setInitialSessions()
    {
        Session::put('companyName', 'Boqueirão Remates');
        Session::put('companyNumber', '55 99937 0070');
        Session::put('companySite', 'www.boqueiraoremates.com.br');
    }

    public function getUserByAuth($login, $password)
    {
        return $this->userRepository->getByAuth($login, $password);
    }

    public function logout(Request $request)
    {
        session()->flush();

        return redirect()
            ->route('loginView')
            ->with('success', 'Log out realizado com sucesso!');
    }

    public function getPin()
    {
        $this->setInitialSessions();
        return view('auth.send-pin');
    }

    public function teste()
    {
        $enderecos = DB::table('endereco')->where('id', '>', 3198)->paginate(350);


        foreach ($enderecos as $endereco) {
            if (isset($endereco->cep)) {
                if (!str_contains($endereco->cep, '.') && !str_contains($endereco->cep, '-')) {
                    // dd($endereco->id);
                    $masked = substr($endereco->cep, 0, 2) . '.' . substr($endereco->cep, 2, 3) . '-' . substr($endereco->cep, 5, 3);

                    DB::table('endereco')
                        ->where('id', $endereco->id)
                        ->update(
                            [
                                'cep' => $masked,
                            ]
                        );
                }
            }
        }

        dd('salvou...');


    }
}

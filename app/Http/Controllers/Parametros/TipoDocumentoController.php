<?php

namespace App\Http\Controllers\Parametros;

use App\Http\Controllers\Controller;

class TipoDocumentoController extends Controller
{
    public function index()
    {
        return view('parametros.tipo-documento');
    }
}

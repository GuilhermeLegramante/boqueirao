<?php

namespace App\Http\Controllers;

use App\Repositories\Cadastros\ClienteRepository;
use Illuminate\Support\Facades\Http;

class MainController extends Controller
{
    public function dashboard()
    {
        $clienteRepository = new ClienteRepository();

        $totalClientes = count($clienteRepository->getAll()->get());
        $totalClientesDivulgacao = $clienteRepository->getTotalClientesDivulgacao();
        $totalHabilitados = $clienteRepository->getTotalHabilitados();

        if ($totalClientes != 0) {
            $percentualHabilitados = ($totalHabilitados * 100) / $totalClientes;
        } else {
            $percentualHabilitados = 0;
        }

        return view('dashboard', compact('totalClientes', 'totalClientesDivulgacao', 'totalHabilitados', 'percentualHabilitados'));
    }
}

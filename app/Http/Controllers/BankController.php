<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BankController extends Controller
{
    public function table()
    {
        return view('parent.bank-table');
    }

    public function form($id = null)
    {
        return view('parent.bank-form', ['id' => $id]);
    }
}

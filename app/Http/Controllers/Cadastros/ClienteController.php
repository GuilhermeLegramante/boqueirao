<?php

namespace App\Http\Controllers\Cadastros;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\ReportFactory;
use App\Repositories\Cadastros\ClienteRepository;
use Barryvdh\DomPDF\Facade as PDF;


class ClienteController extends Controller
{

    private $clienteRepository;

    public function __construct(ClienteRepository $clienteRepo)
    {
        $this->clienteRepository = $clienteRepo;
    }

    public function index()
    {
        return view('cliente.table');
    }

    public function viewInsertForm()
    {
        return view('cliente.form');
    }

    public function viewEditForm($idcliente)
    {
        return view('cliente.edit-form', compact('idcliente'));
    }

    public function getBasicReport($id)
    {
        $cliente = $this->clienteRepository->findById($id);
        $fileName = 'ficha_cadastro_' . $cliente->idcliente;

        $args = [
            'cliente' => $cliente,
            'title' => 'FICHA CADASTRAL',
        ];

        return ReportFactory::getBasicPdf('portrait', 'reports.cliente.detail', $args, $fileName);
    }
}

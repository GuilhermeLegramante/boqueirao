<?php

namespace App\Http\Controllers\Cadastros;

use App\Http\Controllers\Controller;
use App\Repositories\Cadastros\DocumentoRepository;

class DocumentoController extends Controller
{

    private $clienteRepository;

    public function __construct(DocumentoRepository $documentoRepo)
    {
        $this->documentoRepository = $documentoRepo;
    }

    public function index()
    {
        return view('documento.documentos');
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocumentTypeController extends Controller
{
    public function table()
    {
        return view('parent.document-type-table');
    }

    public function form($id = null)
    {
        return view('parent.document-type-form', ['id' => $id]);
    }
}

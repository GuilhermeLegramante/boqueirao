<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = DB::table('usuario')->where('id', Session::get('userId'))->get()->first();

        if ($user->admin == 1) {
            return $next($request);
        } else {
            return redirect()->route('dashboard')->with('error', 'Você não possui permissão de acesso.');
        }
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Saudacoes extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "HardSoft - Cadastro Boqueirão Remates";
    private $nome;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nome)
    {
        $this->nome = $nome;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $nome = $this->nome;
        return $this->markdown('emails.saudacoes', compact('nome'));
       
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use DB;

class NovoProtocolo extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "Boqueirão Remates - Novo Protocolo";
    private $protocolo;
    private $nome;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($protocolo, $nome)
    {
        $this->protocolo = $protocolo;
        $this->nome = $nome;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $protocolo = $this->protocolo;
        $nome = $this->nome;
        
        return $this->markdown('emails.novoprotocolo', compact('nome', 'protocolo'));
       
    }
}
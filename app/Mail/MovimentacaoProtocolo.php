<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MovimentacaoProtocolo extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = "Boqueirão Remates";
    private $numeroProtocolo;
    private $nomeProtocolante;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($numeroProtocolo, $nomeProtocolante)
    {
        $this->numeroProtocolo = $numeroProtocolo;
        $this->nomeProtocolante = $nomeProtocolante;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $protocolo = $this->numeroProtocolo;
        $nomeProtocolante = $this->nomeProtocolante;
        
        return $this->markdown('emails.movimentacao', compact('nomeProtocolante', 'protocolo'));    }
}

<?php

namespace App\Traits;

trait WithDatatable
{
    public $sortBy = 'id';
    public $sortDirection = 'asc';
    protected $paginationTheme = 'bootstrap';
    public $perPage = 30;
    public $searchDesc = '';
    public $searchCod = '';
    public $search = '';

    public function sortBy($field)
    {
        if ($this->sortDirection == 'asc') {
            $this->sortDirection = 'desc';
        } else {
            $this->sortDirection = 'asc';
        }

        return $this->sortBy = $field;
    }

    public function resetFields()
    {
        $this->searchCod = "";
        $this->searchDesc = "";
        $this->perPage = 30;
    }

    public function resetFieldsDynamic($fields)
    {
        $this->reset($fields);
        $this->perPage = 30;
    }

    public function load($value)
    {
        $this->perPage += $value;
    }

}

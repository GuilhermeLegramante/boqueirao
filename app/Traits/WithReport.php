<?php

namespace App\Traits;

trait WithReport
{
    public function report($route)
    {   
        return redirect()->route($route);
    }
}
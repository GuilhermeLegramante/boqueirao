<?php

namespace App\Services;

class Mask
{
    public static function cpfCnpj($value)
    {
        if ((strlen($value) == 11) && (is_numeric($value))) {
            $value = substr($value, 0, 3) . '.' . substr($value, 3, 3) . '.' . substr($value, 6, 3) . '-' . substr($value, 9);
        }

        if ((strlen($value) == 14) && (is_numeric($value))) {
            $value = substr($value, 0, 2) . '.' . substr($value, 2, 3) . '.' . substr($value, 5, 3) . '/' . substr($value, 8, 4) . '-' . substr($value, 12, 2);
        }

        return $value;
    }

    public static function celular($value)
    {
        // 55999181805 (55) 99918-1805
        if ((strlen($value) == 11) && (is_numeric($value))) {
            $value = '(' . substr($value, 0, 2) . ') ' . substr($value, 2, 5) . '-' . substr($value, 7, 4);
        }

        return $value;
    }

    public static function telFixo($value)
    {
        // 5534225896 (55) 3422-5896
        if ((strlen($value) == 10) && (is_numeric($value))) {
            $value = '(' . substr($value, 0, 2) . ') ' . substr($value, 2, 4) . '-' . substr($value, 6, 4);
        }

        return $value;
    }

    public static function normalizeString($value, bool $toUpper = true)
    {
        $temp = trim($value);
        if ($toUpper) {
            $temp = mb_strtoupper($temp, 'UTF-8');
        }

        return $temp;
    }

    public static function money($value)
    {
        if (is_numeric($value)) {
            $value = number_format($value, 2, ',', '.');
        } else {
            $value = preg_replace("/[^0-9,]/", "", $value);
            $value = preg_replace("/,/", ".", $value);
            if (is_numeric($value)) {
                $value = number_format($value, 2, ',', '.');
            }
        }

        return $value;
    }

    public static function cep($value)
    {
        $value = Mask::removeMoneyMask($value);

        $mask = new Mask();

        return $mask->genericMask($value, '##.###-###');
    }

    public static function removeMoneyMask($value)
    {
        // Remove todos os caracteres não numéricos e diferentes de . e ,
        $newValue = preg_replace("/[^0-9.,]/", "", $value);

        if (isset($value) && $value != '') {
            return number_format(str_replace(",", ".", str_replace(".", "", $newValue)), 2, '.', '');
        } else {
            return 0;
        }
    }

    private function genericMask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; ++$i) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) {
                    $maskared .= $val[$k++];
                }
            } else {
                if (isset($mask[$i])) {
                    $maskared .= $mask[$i];
                }
            }
        }

        return $maskared;
    }

    public static function removeCpfCnpj($value)
    {
        return preg_replace('/[^0-9]/', '', $value);
    }
}

<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class TipoDocumentoRepository
{
    private $table = 'tipodocumento';

    private $baseQuery;

    public function __construct()
    {
        $this->baseQuery = DB::table($this->table)
            ->select(
                $this->table . '.id AS id',
                $this->table . '.descricao AS descricao',
            );
    }

    public function getTableName()
    {
        return $this->table;
    }

    public function all()
    {
        return $this->baseQuery;
    }

    public function findById($id)
    {
        return $this->baseQuery->where($this->table . '.id', $id)->get()->first();
    }

    public function save($data)
    {
        try {
            $insert = DB::table($this->table)
                ->insertGetId(
                    [
                        'descricao' => $data['descricao'],
                    ]
                );

            return ['success' => true, 'content' => $insert];

        } catch (\Illuminate\Database\QueryException $error) {
            return ['success' => false, 'content' => config('messages.mysql.' . $error->errorInfo[1]), 'original' => $error->getMessage()];
        }
    }

    public function update($data)
    {
        $array = [];

        foreach ($data as $key => $value) {
            if ($key != 'id') {
                $array[strtolower($key)] = $value;
            }
        }

        try {
            $oldData = $this->findById($data['id']);
            $update = DB::table($this->table)
                ->where('id', $data['id'])
                ->update($array);

            return ['success' => true, 'content' => $update];

        } catch (\Illuminate\Database\QueryException $error) {
            return ['success' => false, 'content' => config('messages.mysql.' . $error->errorInfo[1]), 'original' => $error->getMessage()];
        }
    }

    public function delete($data)
    {
        try {
            $delete = DB::table($this->table)->where('id', $data['id'])->delete();

            return ['success' => true, 'content' => $data['id']];

        } catch (\Illuminate\Database\QueryException $error) {
            return ['success' => false, 'content' => config('messages.mysql.' . $error->errorInfo[1]), 'original' => $error->getMessage()];
        }
    }

}

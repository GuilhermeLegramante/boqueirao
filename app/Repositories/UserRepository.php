<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class UserRepository
{
    private $table = 'usuario';

    public function __construct()
    {
        $this->baseQuery = DB::table($this->table)
            ->select(
                $this->table . '.id AS id',
                $this->table . '.id AS code',
                $this->table . '.nome AS name',
                $this->table . '.login AS login',
                $this->table . '.email AS email',
                $this->table . '.admin AS isAdmin',
            );
    }

    public function all(string $search = null, string $sortBy = 'id', string $sortDirection = 'asc', string $perPage = '30')
    {
        return $this->baseQuery
            ->where([
                [$this->table . '.nome', 'like', '%' . $search . '%'],
            ])
            ->orWhere([
                [$this->table . '.id', 'like', '%' . $search . '%'],
            ])
            ->orWhere([
                [$this->table . '.login', 'like', '%' . $search . '%'],
            ])
            ->orderBy($sortBy, $sortDirection)
            ->paginate($perPage);
    }

    public function allSimplified()
    {
        return $this->baseQuery->get();
    }

    public function save($data)
    {
        return DB::table($this->table)
            ->insertGetId(
                [
                    'nome' => $data['name'],
                    'login' => $data['login'],
                    'senha' => sha1($data['password']),
                    'email' => $data['email'],
                    'admin' => $data['isAdmin'],
                    'created_at' => now(),
                ]
            );
    }

    public function update($data)
    {
        DB::table($this->table)
            ->where('id', $data['recordId'])
            ->update(
                [
                    'nome' => $data['name'],
                    'login' => $data['login'],
                    'senha' => sha1($data['password']),
                    'email' => $data['email'],
                    'admin' => $data['isAdmin'],
                    'updated_at' => now(),
                ]
            );
    }

    public function delete($data)
    {
        DB::table($this->table)
            ->where('id', $data['recordId'])
            ->delete();
    }

    public function findById($id)
    {
        return $this->baseQuery
            ->where($this->table . '.id', $id)
            ->get()
            ->first();
    }

}

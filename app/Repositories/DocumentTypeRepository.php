<?php

namespace App\Repositories;

use App\Repositories\Traits\WithCrud;
use App\Services\LogService;
use Illuminate\Support\Facades\DB;

class DocumentTypeRepository
{
    use WithCrud;

    private $table = 'tipodocumento';

    public function __construct()
    {
        $this->baseQuery = DB::table($this->table)
            ->select(
                $this->table . '.id AS id',
                $this->table . '.id AS code',
                $this->table . '.descricao AS description',
            );
    }

    public function all(string $search = null, string $sortBy = 'id', string $sortDirection = 'asc', string $perPage = '30')
    {
        return $this->baseQuery
            ->where([
                [$this->table . '.descricao', 'like', '%' . $search . '%'],
            ])
            ->orderBy($sortBy, $sortDirection)
            ->paginate($perPage);
    }

    public function allSimplified()
    {
        return $this->baseQuery->get();
    }

    public function save($data)
    {
        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'I',
            date('Y-m-d H:i:s'),
            json_encode($data),
            null
        );

        return DB::table($this->table)
            ->insertGetId(
                [
                    'descricao' => $data['description'],
                    'idusuario' => session()->get('userId'),
                    'created_at' => now(),
                ]
            );
    }

    public function update($data)
    {
        $oldData = $this->findById($data['recordId']);

        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'U',
            date('Y-m-d H:i:s'),
            json_encode($oldData),
            json_encode($data)
        );

        DB::table($this->table)
            ->where('id', $data['recordId'])
            ->update(
                [
                    'descricao' => $data['description'],
                    'updated_at' => now(),
                ]
            );
    }

    public function delete($data)
    {
        $oldData = $this->findById($data['recordId']);

        LogService::saveLog(
           session()->get('userId'),
           $this->table,
           'D',
           date('Y-m-d H:i:s'),
           json_encode($oldData),
           null
       );

        DB::table($this->table)
            ->where('id', $data['recordId'])
            ->delete();
    }
}

<?php

namespace App\Repositories;

use App\Repositories\Traits\WithCrud;
use App\Services\LogService;
use Illuminate\Support\Facades\DB;

class BankRepository
{
    use WithCrud;

    private $table = 'banco';

    public function __construct()
    {
        $this->baseQuery = DB::table($this->table)
            ->select(
                $this->table . '.id AS id',
                $this->table . '.cod AS code',
                $this->table . '.banco AS description',
            );
    }

    public function all(string $search = null, string $sortBy = 'id', string $sortDirection = 'asc', string $perPage = '30')
    {
        return $this->baseQuery
            ->where([
                [$this->table . '.banco', 'like', '%' . $search . '%'],
            ])
            ->orderBy($sortBy, $sortDirection)
            ->paginate($perPage);
    }

    public function allSimplified()
    {
        return $this->baseQuery->get();
    }

    public function save($data)
    {
        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'I',
            date('Y-m-d H:i:s'),
            json_encode($data),
            null
        );

        return DB::table($this->table)
            ->insertGetId(
                [
                    'idusuario' => session()->get('userId'),
                    'cod' => $data['code'],
                    'banco' => $data['description'],
                    'datahora' => date('Y-m-d H:i:s'),
                ]
            );
    }

    public function update($data)
    {
        $oldData = $this->findById($data['recordId']);

        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'U',
            date('Y-m-d H:i:s'),
            json_encode($oldData),
            json_encode($data)
        );

        DB::table($this->table)
            ->where('id', $data['recordId'])
            ->update(
                [
                    'idusuario' => session()->get('userId'),
                    'cod' => $data['code'],
                    'banco' => $data['description'],
                    'datahora' => date('Y-m-d H:i:s'),
                ]
            );
    }

    public function delete($data)
    {
        $oldData = $this->findById($data['recordId']);

        LogService::saveLog(
           session()->get('userId'),
           $this->table,
           'D',
           date('Y-m-d H:i:s'),
           json_encode($oldData),
           null
       );

        DB::table($this->table)
            ->where('id', $data['recordId'])
            ->delete();
    }

}

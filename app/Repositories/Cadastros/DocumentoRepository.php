<?php

namespace App\Repositories\Cadastros;

use App\Services\LogService;
use Illuminate\Support\Facades\DB;
class DocumentoRepository
{
    private $table = 'documento';

    public function getAll()
    {
        return DB::table('documento')
            ->join('cliente', 'cliente.id', '=', 'documento.idcliente')
            ->join('tipodocumento', 'tipodocumento.id', '=', 'documento.idtipodocumento')
            ->join('usuario', 'usuario.id', '=', 'documento.idusuario')
            ->select(
                'documento.id AS iddocumento',
                'documento.path',
                'documento.datainclusao',
                'documento.idcliente',
                'documento.idtipodocumento',
                'documento.idusuario',
                'cliente.nome AS nomecliente',
                'tipodocumento.descricao AS descricaotipodocumento',
                'usuario.nome AS nomeusuario',
            );
    }

    public function getAllTiposDocumento()
    {
        return DB::table('tipodocumento')->get();
    }

    public function findById($id)
    {
        return DB::table('documento')
            ->join('cliente', 'cliente.id', '=', 'documento.idcliente')
            ->join('tipodocumento', 'tipodocumento.id', '=', 'documento.idtipodocumento')
            ->join('usuario', 'usuario.id', '=', 'documento.idusuario')
            ->select(
                'documento.id AS iddocumento',
                'documento.path',
                'documento.datainclusao',
                'documento.idcliente',
                'documento.idtipodocumento',
                'documento.idusuario',
                'cliente.nome AS nomecliente',
                'tipodocumento.descricao AS descricaotipodocumento',
                'usuario.nome AS nomeusuario',
            )
            ->where('documento.id', $id)->get()->first();
    }

    public function findByClienteId($idcliente)
    {
        return DB::table('documento')
            ->join('cliente', 'cliente.id', '=', 'documento.idcliente')
            ->join('tipodocumento', 'tipodocumento.id', '=', 'documento.idtipodocumento')
            ->join('usuario', 'usuario.id', '=', 'documento.idusuario')
            ->select(
                'documento.id AS iddocumento',
                'documento.path',
                'documento.datainclusao',
                'documento.idcliente',
                'documento.idtipodocumento',
                'documento.idusuario',
                'cliente.nome AS nomecliente',
                'tipodocumento.descricao AS descricaotipodocumento',
                'usuario.nome AS nomeusuario',
            )
            ->where('cliente.id', $idcliente)->get();
    }

    public function create($data)
    {
        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'I',
            date('Y-m-d H:i:s'),
            json_encode($data),
            null
        );

        DB::beginTransaction();
        try {

            $documento = DB::table('documento')->insertGetId(
                [
                    'idtipodocumento' => $data['idtipodocumento'],
                    'idcliente' => $data['idcliente'],
                    'path' => $data['path'],
                    'datainclusao' => $data['datainclusao'],
                    'idusuario' => $data['idusuario'],
                ]
            );

            DB::commit();
        } catch (\Illuminate\Database\QueryException $error) {
            DB::rollback();
            dd($error->getMessage());
        }

        return $documento;

    }

    public function delete($id)
    {
        $oldData = $this->findById($id);

        LogService::saveLog(
           session()->get('userId'),
           $this->table,
           'D',
           date('Y-m-d H:i:s'),
           json_encode($oldData),
           null
       );

        return DB::table('documento')->where('id', $id)->delete();
    }

    public function update($data)
    {
        $oldData = $this->findById($data['iddocumento']);

        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'U',
            date('Y-m-d H:i:s'),
            json_encode($oldData),
            json_encode($data)
        );

        DB::beginTransaction();
        try {

            $update = DB::table('documento')
                ->where('documento.id', $data['iddocumento'])
                ->update(
                    [
                        'idtipodocumento' => $data['idtipodocumento'],
                        'idcliente' => $data['idcliente'],
                        'path' => $data['path'],
                        'datainclusao' => $data['datainclusao'],
                        'idusuario' => $data['idusuario'],
                    ]
                );

            DB::commit();
        } catch (\Illuminate\Database\QueryException $error) {
            DB::rollback();
            dd($error->getMessage());
        }

        return $update;
    }

}

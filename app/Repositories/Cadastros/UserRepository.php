<?php

namespace App\Repositories\Cadastros;

use Illuminate\Support\Facades\DB;

class UserRepository
{
    public function getByAuth($login, $password)
    {
        $data = DB::table('usuario')
            ->where('usuario.login', '=', $login)
            ->where('usuario.senha', '=', $password)
            ->get()->first();

        return $data;
    }

}

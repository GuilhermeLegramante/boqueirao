<?php

namespace App\Repositories\Cadastros;

use App\Services\LogService;
use Illuminate\Support\Facades\DB;


class ClienteRepository
{
    private $table = 'cliente';

    private $baseQuery;

    public function __construct()
    {
        $this->baseQuery = DB::table('cliente')
            ->leftJoin('endereco', 'endereco.id', '=', 'cliente.idendereco')
            ->leftJoin('banco', 'banco.id', '=', 'cliente.idbanco')
            ->select(
                'cliente.id AS idcliente',
                'cliente.canalinclusao',
                'cliente.nome',
                'cliente.email',
                'cliente.cpfcnpj',
                'cliente.rg',
                'cliente.nomemae',
                'cliente.nomepai',
                'cliente.datanascimento',
                'cliente.agencia',
                'cliente.conta',
                'cliente.profissao',
                'cliente.obsprofissao',
                'cliente.renda',
                'cliente.cadastroleiloeira',
                'cliente.telefonecomercial',
                'cliente.telefoneresidencial',
                'cliente.telefonecelular',
                'cliente.celularwhats',
                'cliente.situacao',
                'cliente.canalinclusao',
                'cliente.leiloeira',
                'cliente.estabelecimento',
                'cliente.datacadastro',
                'cliente.dataalteracao',
                'cliente.observacao',
                'cliente.perfil',
                'banco.id AS idbanco',
                'banco.banco AS descricaobanco',
                'banco.cod AS codbanco',
                'endereco.id AS endereco',
                'endereco.logradouro',
                'endereco.complemento',
                'endereco.numero',
                'endereco.bairro',
                'endereco.cep',
                'endereco.cidade',
                'endereco.uf',
                'endereco.pais'
            );
    }

    public function getAll()
    {
        return $this->baseQuery;
    }

    public function getTotalHabilitados()
    {
        return DB::table('cliente')->where('situacao', 'HABILITADO')->count();
    }

    public function getTotalClientesDivulgacao()
    {
        return DB::table('cliente')->where('canalinclusao', 'DIVULGACAO')->count();
    }


    public function getAllLeiloeiras()
    {
        return DB::table('leiloeira')->get();
    }

    public function getAllEstabelecimentos()
    {
        return DB::table('estabelecimento')->get();
    }

    public function getAllFaixasRenda()
    {
        return DB::table('faixarenda')->get();
    }

    public function getAllBancos()
    {
        return DB::table('banco')->get();
    }

    public function findById($id)
    {
        return $this->baseQuery->where('cliente.id', $id)->get()->first();
    }

    public function create($data)
    {
        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'I',
            date('Y-m-d H:i:s'),
            json_encode($data),
            null
        );

        DB::beginTransaction();
        try {

            $endereco = DB::table('endereco')->insertGetId(
                [
                    'cep' => $data['cep'],
                    'logradouro' => $data['logradouro'],
                    'complemento' => $data['complemento'],
                    'numero' => $data['numero'],
                    'bairro' => $data['bairro'],
                    'cidade' => $data['cidade'],
                    'uf' => $data['uf'],
                ]
            );

            $cliente = DB::table('cliente')->insertGetId(
                [
                    'nome' => $data['nome'],
                    'nomemae' => $data['nomemae'],
                    'nomepai' => $data['nomepai'],
                    'email' => $data['email'],
                    'cpfcnpj' => $data['cpfcnpj'],
                    'rg' => $data['rg'],
                    'datanascimento' => $data['datanascimento'],
                    'idendereco' => $endereco,
                    'idbanco' => $data['banco'],
                    'estabelecimento' => $data['estabelecimento'],
                    'renda' => $data['renda'],
                    'cadastroleiloeira' => $data['cadastroleiloeira'],
                    'leiloeira' => $data['leiloeira'],
                    'telefonecomercial' => $data['telefonecomercial'],
                    'telefoneresidencial' => $data['telefoneresidencial'],
                    'telefonecelular' => $data['telefonecelular'],
                    'celularwhats' => $data['celularwhats'],
                    'agencia' => $data['agencia'],
                    'conta' => $data['conta'],
                    'profissao' => $data['profissao'],
                    'obsprofissao' => $data['obsprofissao'],
                    'situacao' => $data['situacao'],
                    'canalinclusao' => $data['canalinclusao'],
                    'datacadastro' => $data['datacadastro'],
                    'dataalteracao' => $data['dataalteracao'],
                    'idusuario' => $data['idusuario'],
                    'observacao' => $data['observacao'],
                    'perfil' => isset($data['perfil']) ? $data['perfil'] : null,
                ]
            );

            DB::commit();
        } catch (\Illuminate\Database\QueryException $error) {
            DB::rollback();
            dd($error->getMessage());
        }

        return $cliente;
    }

    public function documentos($id)
    {
        return  DB::table('documento')
            ->join('tipodocumento', 'tipodocumento.id', '=', 'documento.idtipodocumento')
            ->select(
                'documento.id',
                'documento.path',
                'tipodocumento.descricao AS descricaoTipoDocumento',
            )
            ->where('documento.idcliente', $id)
            ->get();
    }

    public function delete($id)
    {
        $oldData = $this->findById($id);

        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'D',
            date('Y-m-d H:i:s'),
            json_encode($oldData),
            null
        );

        DB::beginTransaction();
        try {
            $docDelete = DB::table('documento')->where('idcliente', $id)->delete();

            $delete = DB::table('cliente')->where('id', $id)->delete();
            DB::commit();
        } catch (\Illuminate\Database\QueryException $error) {
            DB::rollback();
            dd($error->getMessage());
        }

        return $delete;
    }

    public function update($data)
    {
        $oldData = $this->findById($data['idcliente']);

        LogService::saveLog(
            session()->get('userId'),
            $this->table,
            'U',
            date('Y-m-d H:i:s'),
            json_encode($oldData),
            json_encode($data)
        );

        DB::beginTransaction();
        try {

            $udpdateEndereco = DB::table('endereco')
                ->where('id', $data['endereco'])
                ->update(
                    [
                        'cep' => $data['cep'],
                        'logradouro' => $data['logradouro'],
                        'complemento' => $data['complemento'],
                        'numero' => $data['numero'],
                        'bairro' => $data['bairro'],
                        'cidade' => $data['cidade'],
                        'uf' => $data['uf'],
                    ]
                );

            $updateCliente = DB::table('cliente')
                ->where('cliente.id', $data['idcliente'])
                ->update(
                    [
                        'nome' => $data['nome'],
                        'nomemae' => $data['nomemae'],
                        'nomepai' => $data['nomepai'],
                        'email' => $data['email'],
                        'cpfcnpj' => $data['cpfcnpj'],
                        'rg' => $data['rg'],
                        'datanascimento' => $data['datanascimento'],
                        'idendereco' => $data['endereco'],
                        'idbanco' => $data['banco'],
                        'estabelecimento' => $data['estabelecimento'],
                        'renda' => $data['renda'],
                        'cadastroleiloeira' => $data['cadastroleiloeira'],
                        'leiloeira' => $data['leiloeira'],
                        'telefonecomercial' => $data['telefonecomercial'],
                        'telefoneresidencial' => $data['telefoneresidencial'],
                        'telefonecelular' => $data['telefonecelular'],
                        'celularwhats' => $data['celularwhats'],
                        'agencia' => $data['agencia'],
                        'conta' => $data['conta'],
                        'profissao' => $data['profissao'],
                        'obsprofissao' => $data['obsprofissao'],
                        'situacao' => $data['situacao'],
                        'canalinclusao' => $data['canalinclusao'],
                        'dataalteracao' => $data['dataalteracao'],
                        'idusuario' => $data['idusuario'],
                        'observacao' => $data['observacao'],
                        'perfil' => isset($data['perfil']) ? $data['perfil'] : null,
                    ]
                );

            DB::commit();
        } catch (\Illuminate\Database\QueryException $error) {
            DB::rollback();
            dd($error->getMessage());
        }

        return $updateCliente;
    }
}

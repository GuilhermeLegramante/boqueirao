<?php

namespace App\Repositories\Traits;

trait WithCrud
{
    private $baseQuery;

    
    public function findById($id)
    {
        return $this->baseQuery
            ->where($this->table . '.id', $id)
            ->get()
            ->first();
    }

}
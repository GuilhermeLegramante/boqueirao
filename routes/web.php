<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;


// ROTAS DE AUTENTICAÇÃO
Route::get('/login', 'Auth\LoginController@loginView')->name('loginView');
Route::get('/esqueci-minha-senha', 'Auth\LoginController@resetPasswordView')->name('resetPasswordView');
Route::post('/esqueci-minha-senha', 'Auth\LoginController@resetPassword')->name('resetPassword');
Route::get('/buscar-pin', 'Auth\LoginController@getPin')->name('getPin');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/sair', 'Auth\LoginController@logout')->name('logout');

Route::get('/teste', 'Auth\LoginController@teste')->name('teste');


// ROTAS SOB AUTENTICAÇÃO
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'MainController@dashboard')->name('dashboard');

    Route::get('/clientes', 'Cadastros\ClienteController@index')->name('clientes');
    Route::get('/clientes/cadastro', 'Cadastros\ClienteController@viewInsertForm')->name('clientes.viewInsertForm');
    Route::get('/clientes/{idcliente}/edicao/', 'Cadastros\ClienteController@viewEditForm')->name('clientes.viewEditForm');
    Route::get('/clientes/{id}/pdf', 'Cadastros\ClienteController@getBasicReport')->name('clientes.getBasicReport');

    Route::get('/documentos', 'Cadastros\DocumentoController@index')->name('documentos');

    Route::group(['middleware' => ['isAdmin']], function () {
        Route::prefix('/tipo-de-documento')->group(function () {
            Route::get('/', 'DocumentTypeController@table')->name('document-type.table');
            Route::get('/inclusao', 'DocumentTypeController@form')->name('document-type.create');
            Route::get('/{id}', 'DocumentTypeController@form')->name('document-type.edit');
        });

        Route::prefix('/banco')->group(function () {
            Route::get('/', 'BankController@table')->name('bank.table');
            Route::get('/inclusao', 'BankController@form')->name('bank.create');
            Route::get('/{id}', 'BankController@form')->name('bank.edit');
        });

        Route::prefix('/usuario')->group(function () {
            Route::get('/', 'UserController@table')->name('user.table');
            Route::get('/inclusao', 'UserController@form')->name('user.create');
            Route::get('/{id}', 'UserController@form')->name('user.edit');
        });
    });

});
